import { Component, Inject, OnInit } from '@angular/core';
import { navItemsContabilidade } from '../../_nav_contabilidade';
import { navItemsEmpresa } from '../../_nav_empresa';
import { navItemsAdministracao } from '../../_nav_administracao';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { TokenService } from '../../services/token.service';
import { RegisterService } from '../../services/register.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {

  public adminProfile: Boolean = false;
  public empresas;
  public contabilidades;
  public navItems;
  public user$: Observable<User>;
  public sidebarMinimized= false;

  constructor(
    private tokenService: TokenService,
    private userService: UserService,
    private router: Router,
    private contabilidadeService: RegisterService
  ) {
    this.user$ = userService.getUser();

    this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationStart) {  
          this.tokenService.isTokenExpired();
          //console.log(this.user$);
            // Show loading indicator
            
        }

        if (event instanceof NavigationEnd) {           
            // Hide loading indicator
        }

        if (event instanceof NavigationError) {
            // Hide loading indicator
            // Present error to user
            console.log(event.error);
            location.reload();
        }
    });
  }

  logout(){
    this.userService.logout();
    this.router.navigate(['']);
  }

  comeBackAdmin(){
    this.adminProfile = false;
    this.empresas = false;
    this.userService.getIdentityAndSetExpires();
    this.router.navigate(['']);
  }

  contabilidadeSelected(id){
    this.userService.getIdentityAndSetExpires(id);  
    this.contabilidadeService.getEmpresasRefetenteAContabilidade(id).subscribe(data=>{
      this.empresas = data;
    })
    this.router.navigate(['']);
  }
  
  empresaSelected(id){
    this.router.navigate(['contabilidade/gerenciar-dashboards/visualizar/'+id]);
  }
  
  ngOnInit() {

    this.user$.subscribe(data => {
      if (data){
        if(data.adminProfile) this.adminProfile = true         
        if (data.Contabilidade != null) {
          this.navItems = navItemsContabilidade;
          this.contabilidadeService.getEmpresasRefetenteAContabilidade(data.Contabilidade).subscribe(data=>{
            this.empresas = data;
          })
        } 

        if (data.Empresa != null) 
          this.navItems = navItemsEmpresa;
        
        if (data.Empresa == data.Contabilidade) {
          this.navItems = navItemsAdministracao;
          this.contabilidadeService.getTodasContabilidades().subscribe(data=>{
            this.contabilidades = data;
          })
        }             
      }
    })
    $(document).ready(function(){
      $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
        $(this).toggleClass('open');
      });

      // Icones
      $('#menuLaco').prepend( $('.navbar-toggler')[1] );
      $('.navbar-toggler-icon')[2].classList.add('second-icon');
      $('.navbar-toggler')[2].addEventListener('click', function(ev){
        $(this).toggleClass('open-menu-icon');
      });
      if ( window.innerWidth < 991 ){
        $('.navbar-toggler-icon')[1].classList.add('d-none');
      }

      //Ajustes Sidebar 
      $('.sidebar-minimizer').on('click', function(ev){
        $('.btn_treinamentos').toggleClass('ajuste_minized-btns');

        ( $('.btn_treinamentos a').text().replace(/\s/g, '') == 'Treinamentos' ) ?
          $('.btn_treinamentos a').text('T') : $('.btn_treinamentos a').text('Treinamentos');
        
        ( $('.btn_suporte a').text().replace(/\s/g, '') == 'SuporteOnline' ) ?
          $('.btn_suporte a').text('S') : $('.btn_suporte a').text('Suporte Online');
     
        $('.btn_suporte').toggleClass('ajuste_minized-btns');
  
        
        $('.btn_treinamentos a').hover(function() {
          if ( $('.btn_suporte').hasClass('ajuste_minized-btns') ) 
            $('.btn_treinamentos a').text('Treinamentos')           
        });
        $('.btn_treinamentos a').mouseleave(function() {
          if ( $('.btn_suporte').hasClass('ajuste_minized-btns') ) 
            $('.btn_treinamentos a').text('T')
        });

        $('.btn_suporte a').hover(function() {
          if ( $('.btn_suporte').hasClass('ajuste_minized-btns') ) 
            $('.btn_suporte a').text('Suporte Online')
        });
        $('.btn_suporte a').mouseleave(function() {
          if ( $('.btn_suporte').hasClass('ajuste_minized-btns') ) 
            $('.btn_suporte a').text('S')
        });

        

      });

    });
  }
}
