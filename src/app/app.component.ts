import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    //http: HttpClient

    ) {  }

  ngOnInit() {    

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationStart) {    
        console.log()    
        return;
      }
      
      if (!(evt instanceof NavigationEnd)) {        
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
