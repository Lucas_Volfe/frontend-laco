export class User {
    id: number;
    username: string;
    password: string;
    email: string;   
    Empresa: number;
    Contabilidade: number;
    Role: number;
    adminProfile: Boolean;
    nome_fantasia: string;  
    foto: string; 

}
