export class BdUpload {
    id: number;
    Ano: string = '';
    Mes: string = '';
    Data_Upload: Date =  new Date();
    Empresa: number = 0;

    /* ------------ Receitas Operacionais --------------- */
    Receitas_Operacionais: number = 0;
    Receita_De_Vendas: number = 10;
    Receita_De_Fretes_E_Entregas: number = 0;
    Outras_Receitas_1: number = 0;
    Outras_Receitas_2: number = 0;    
    /* ------------ Fim Receitas Operacionais --------------- */

    /* ------------ Deduções da Receita Bruta --------------- */
    Deducoes_Da_Receita_Bruta: number = 0;
    Impostos_Sobre_Vendas: number = 0;
    Comissoes_Sobre_Vendas: number = 0;
    Descontos_Incondicionais: number = 0;
    Devolucoes_De_Vendas: number = 0;
    Outras_Deducoes_1: number = 0;
    Outras_Deducoes_2: number = 0;
    /* ------------ Fim Deduções da Receita Bruta --------------- */

    /* ------------ Receita LÌquida de Vendas --------------- */
    Receita_Liquida_De_Vendas: number = 0;
    /* ------------ Fim Receita LÌquida de Vendas --------------- */

    /* ------------ Custos Operacionais --------------- */
    Custos_Operacionais: number = 0;
    Custo_Das_Mercadorias_Vendidas: number = 0;
    Custo_Dos_Produtos_Vendidos: number = 0;
    Custo_Dos_Servicos_Prestados: number = 0;
    Outros_Custos: number = 0;
    /* ------------ Fim Custos Operacionais --------------- */

    /* ------------ Lucro Bruto --------------- */
    Lucro_Bruto: number = 0;
    /* ------------ Fim Lucro Bruto --------------- */
    
    /* ------------ Despesas Operacionais --------------- */
    Despesas_Operacionais: number = 0;
    Despesas_Com_Vendas: number = 0;
    Despesas_Administrativas: number = 0;
    Despesas_Tributarias_Gerais: number = 0;
    Outras_Despesas_1: number = 0;
    Outras_Despesas_2: number = 0;
    Outras_Despesas_3: number = 0;
    Outras_Despesas_4: number = 0;
    /* ------------ Fim Despesas Operacionais --------------- */

    /* ------------ Lucro / PrejuÌzo Operacional --------------- */
    Lucro_Prejuizo_Operacional: number = 0;
    /* ------------ Fim Lucro / PrejuÌzo Operacional --------------- */

    /* ------------ Receitas e Despesas Financeiras --------------- */
    Receitas_E_Despesas_Financeiras: number = 0;
    Receitas_E_Rendimentos_Financeiros: number = 0;
    Despesas_Financeiras: number = 0;
    Outras_Receitas_Financeiras: number = 0;
    Outras_Despesas_Financeiras: number = 0;
    /* ------------ Fim Receitas e Despesas Financeiras --------------- */

    /* ------------ Outras Receitas e Despesas Não Operacionais --------------- */
    Outras_Receitas_E_Despesas_Nao_Operacionais: number = 0;
    Outras_Receitas_Nao_Operacionais: number = 0;
    Outras_Despesas_Nao_Operacionais: number = 0;
    /* ------------ Fim Outras Receitas e Despesas Não Operacionais --------------- */

    /* ------------ Lucro / PrejuÌzo Liquido --------------- */
    Lucro_Prejuizo_Liquido: number = 0;
    /* ------------ Fim Lucro / PrejuÌzo Liquido --------------- */

    /* ------------   Desembolso com Investimentos e Empréstimos   --------------- */
    Desembolso_Com_Investimentos_E_Emprestimos: number = 0;
    Investimentos_Em_Imobilizado: number = 0;
    Emprestimos_E_Dividas: number = 0;
    Outros_Investimentos_E_Emprestimos: number = 0;
    /* ------------ Fim Desembolso com Investimentos e Empréstimos --------------- */

    /* ------------ Lucro / PrejuÌzo Final --------------- */
    Lucro_Prejuizo_Final: number = 0;



    //SOMAS EXCEL - CONFERIR E REVISAO, TEM VALORES NAO FECHANDO
    somasCelulasFormulasExcel(coluna: BdUpload){        
/*
        coluna.ReceitasOperacionais = 
          coluna.ReceitaDeVendas +
          coluna.ReceitaDeFretesEntregas +
          coluna.OutrasReceitas_1 +
          coluna.OutrasReceitas_2;
    
        coluna.DeducoesDaReceitaBruta = -1 * (
          coluna.ImpostosSobreVendas +
          coluna.ComissoesSobreVendas +
          coluna.DescontosIncondicionais +
          coluna.DevolucoesDeVendas +
          coluna.Deducoes_1);
    
        coluna.ReceitaLiquidaDeVendas = 
          coluna.ReceitasOperacionais + 
          coluna.DeducoesDaReceitaBruta
    
        coluna.CustosOperacionais = -1 * (
          coluna.CustoDosProdutosVendidos + 
          coluna.CustoDasVendasDeProdutos +
          coluna.CustoDosServicosPrestados +
          coluna.OutrosCustos);
        
        coluna.LucroBruto = 
          coluna.ReceitaLiquidaDeVendas +
          coluna.CustosOperacionais;
    
        coluna.DespesasOperacionais_somas = -1 * (
          coluna.DespesasComerciais +
          coluna.DespesasAdministrativas +
          coluna.DespesasOperacionais +
          coluna.PerdasEstoque +
          coluna.INSSeFGTS +
          coluna.OutrasDespesas_3 +
          coluna.OutrasDespesas_4);
    
        coluna.LucroPrejuizoOperacional =
          coluna.LucroBruto + 
          coluna.DespesasOperacionais_somas
    
          //ANALISAR QUE NO EXCEL TEM UM CAMPO QUE PODE SER + ou -
        coluna.ReceitasDespesasFinanceiras =
          coluna.ReceitasRendimentosFinanceiros -
          coluna.DespesasFinanceiras;//+ ou - coluna.OutrosReceitasDespesasFinanceiras
    
        coluna.OutrasReceitasDespesasNaoOperacionais =
          coluna.OutrasReceitasNaoOperacionais -
          coluna.OutrasDespesasNaoOperacionais;
    
        coluna.LucroPrejuizoLiquido =
          coluna.LucroPrejuizoOperacional +
          coluna.ReceitasDespesasFinanceiras +
          coluna.OutrasReceitasDespesasNaoOperacionais;
    
        coluna.DespesasComInvestimentosEmprestimos = -1 * (
          coluna.InvestimentosEmImobilizado +
          coluna.Emprestimos_Dividas +
          coluna.OutrosInvestimentosEmprestimos);
    
        coluna.LucrosPrejuizoFinal = 
          coluna.LucroPrejuizoLiquido +
          coluna.DespesasComInvestimentosEmprestimos
    */
        return coluna
    }

    somasColunaTotal(coluna: BdUpload){
      
    }

}

    