export class MesesNumero{        
    meses = [
        { mes: "janeiro", abreviacao: "jan", valor: 1 },
        { mes: "fevereiro", abreviacao: "fev", valor: 2},
        { mes: "marco", abreviacao: "mar", valor: 3},
        { mes: "abril", abreviacao: "abr", valor: 4},
        { mes: "maio", abreviacao: "mai", valor: 5},
        { mes: "junho", abreviacao: "jun", valor: 6},
        { mes: "julho", abreviacao: "jul", valor: 7},
        { mes: "agosto", abreviacao: "ago", valor: 8},
        { mes: "setembro", abreviacao: "set", valor: 9},
        { mes: "outubro", abreviacao: "out", valor: 10},
        { mes: "novembro", abreviacao: "nov", valor: 11},
        { mes: "dezembro", abreviacao: "dez", valor: 12}
    ]
}