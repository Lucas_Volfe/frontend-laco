import { ObjtSerie } from './objtSerie';

export class ConjuntoObjtSerie{
    constructor(name, serie){
        this.name = name;
        this.series = serie;
    }
    name: string; 
    series:ObjtSerie[];
}