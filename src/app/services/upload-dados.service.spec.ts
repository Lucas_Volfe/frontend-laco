import { TestBed } from '@angular/core/testing';

import { UploadDadosService } from './upload-dados.service';

describe('UploadDadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadDadosService = TestBed.get(UploadDadosService);
    expect(service).toBeTruthy();
  });
});
