import { TestBed } from '@angular/core/testing';

import { HistoricoUploadsBpService } from './historico-uploads-bp.service';

describe('HistoricoUploadsBpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoricoUploadsBpService = TestBed.get(HistoricoUploadsBpService);
    expect(service).toBeTruthy();
  });
});
