import { TestBed } from '@angular/core/testing';

import { HistoricoUploadsDreService } from './historico-uploads-dre.service';

describe('HistoricoUploadsDreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoricoUploadsDreService = TestBed.get(HistoricoUploadsDreService);
    expect(service).toBeTruthy();
  });
});
