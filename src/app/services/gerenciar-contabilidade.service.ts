import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../environments/environment'
import { Service } from './service'
import { Contabilidade } from '../models/contabilidade';

const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class GerenciarContabilidadeService extends Service{

    constructor(private http: HttpClient ) {
        super();
    }

    getContabilidades(): Observable<Contabilidade[]>{
        return this.http.get<Contabilidade[]>(API + 'contabilidade');
    }

   
}