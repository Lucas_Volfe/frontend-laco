import { TestBed } from '@angular/core/testing';

import { CalculosDreService } from './calculos-dre.service';

describe('CalculosDreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculosDreService = TestBed.get(CalculosDreService);
    expect(service).toBeTruthy();
  });
});
