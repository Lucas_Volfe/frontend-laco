import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

const KEY = 'authToken';

@Injectable({
  providedIn: 'root'
})
export class TokenService {  

  private DATE_EXPIRES:Date;

  constructor(
    private router:Router
  ){}

  hasToken() {
      return !!this.getToken();
  }

  setToken(token) {
      window.localStorage.setItem(KEY, token);
  }

  getToken() {
      return window.localStorage.getItem(KEY);
  }

  removeToken() {
      window.localStorage.removeItem(KEY);
  }  

  setExpireDate(date: Date){
    this.DATE_EXPIRES = new Date(date);
  }
  
  isTokenExpired(token?: string): boolean {    
    if(!token) token = this.getToken();
    if(!token) return true;
    if(this.DATE_EXPIRES === undefined) return false;  
    if (this.DATE_EXPIRES.getTime() < new Date().getTime()){
      this.removeToken();
      this.router.navigate(['']);
      location.reload();
    }
    return true;
  }
}
