import { TestBed } from '@angular/core/testing';

import { GerenciarClientesService } from './gerenciar-clientes.service';

describe('GerenciarClientesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GerenciarClientesService = TestBed.get(GerenciarClientesService);
    expect(service).toBeTruthy();
  });
});
