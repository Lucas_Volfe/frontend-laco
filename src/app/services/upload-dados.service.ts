import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Service } from './service';
import { environment } from './../../environments/environment'
import { BdUpload } from '../models/bd-upload';
import { HistoricoUploads } from '../models/historico-uploads';
import { Table } from '../models/table';
import { BalancoPatrimonial } from '../models/balanco-patrimonial';

const API = environment.ApiUrl;

@Injectable({
  providedIn: 'root'
})
export class UploadDadosService extends Service{

  constructor(private http: HttpClient ) {
      super();
  }

  create(itens: BdUpload[]): Observable<BdUpload[]>{  
    return this.http.post<BdUpload[]>( API + 'uploads', itens);
  }

  createBalancoPatrimonial(iten: BalancoPatrimonial): Observable<BalancoPatrimonial>{  
    return this.http.post<BalancoPatrimonial>( API + 'balanco-patrimonial', iten);
  }

  getEmpresasPorContabilidade(idContabilidade): Observable<Table[]>{
    return this.http.get<Table[]>(API + 'um-registro-empresa-por-contabilidade/' + idContabilidade);
  }

  getDisponibilidadeUpload(empresa) {  
    return this.http.get( API + 'check-disponibilidade/' + empresa);
  }

  getHistoricoUploads(empresa): Observable<HistoricoUploads[]> {  
    return this.http.get<HistoricoUploads[]>( API + 'historico-uploads/' + empresa);
  }
 
}
