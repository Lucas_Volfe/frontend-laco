import { TestBed } from '@angular/core/testing';

import { GerenciarDashboardsService } from './gerenciar-dashboards.service';

describe('GerenciarDashboardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GerenciarDashboardsService = TestBed.get(GerenciarDashboardsService);
    expect(service).toBeTruthy();
  });
});
