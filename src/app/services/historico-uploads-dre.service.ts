import { Injectable } from '@angular/core';
import { Service } from './service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const API = environment.ApiUrl;


@Injectable({
  providedIn: 'root'
})
export class HistoricoUploadsDreService extends Service{

  constructor(
    private http: HttpClient,    
    ) {
      super();
  }

  getHistoricoUpload(empresa) {  
    return this.http.get( API + 'historico-uploads/' + empresa);
  }
}
