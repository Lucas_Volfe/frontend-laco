import { TestBed } from '@angular/core/testing';

import { DashboardsAdminService } from './dashboards-admin.service';

describe('DashboardsAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashboardsAdminService = TestBed.get(DashboardsAdminService);
    expect(service).toBeTruthy();
  });
});
