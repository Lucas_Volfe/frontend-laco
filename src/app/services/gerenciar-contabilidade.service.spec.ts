import { TestBed } from '@angular/core/testing';

import { GerenciarContabilidadeService } from './gerenciar-contabilidade.service';

describe('GerenciarContabilidadeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GerenciarContabilidadeService = TestBed.get(GerenciarContabilidadeService);
    expect(service).toBeTruthy();
  });
});
