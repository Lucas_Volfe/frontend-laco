import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Service } from './service';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { Empreendimento } from '../models/empreendimento';
import { Contabilidade } from '../models/contabilidade';

const API = environment.ApiUrl;

@Injectable()
export class RegisterService extends Service{

  constructor(
    private http: HttpClient,    
    ) {
      super();
  }

  checkUserNameTaken(user: string) {
    return this.http.get(API + 'check-user-name?new_user=' + user);
  }

  checkEmailTaken(email: string) {
    return this.http.get(API + 'check-email?email=' + email);
  }

  createUser(novoUsuario) {
    return this.http.post( API + 'usuario', novoUsuario);    
  }

  createEmpresa(novaEmpresa, foto) {
    let formData = this.montFormData(novaEmpresa);
    formData.append('Contabilidade', novaEmpresa.Contabilidade);
    formData.append('tem_upload', novaEmpresa.tem_upload);
    formData.append('foto', foto);
    return this.http.post( API + 'empresa', formData);    
  }

  getEmpresa(idEmpresa) {
    return this.http.get( API + 'empresa/'+ idEmpresa);    
  }

  getContabilidade(idContabilidade): Observable<Contabilidade> {
    return this.http.get<Contabilidade>( API + 'contabilidade/'+ idContabilidade);    
  }

  getEmpresas(): Observable<Empreendimento[]> {
    return this.http.get<Empreendimento[]>( API + 'empresa');    
  }

  getEmpresasRefetenteAContabilidade(idContabilidade){
    return this.http.get(API + 'um-registro-empresa-por-contabilidade/' + idContabilidade );
  }
  
  getTodasContabilidades(): Observable<Empreendimento[]> {
    return this.http.get<Empreendimento[]>(API + 'contabilidade' );
  }

  createContabilidade(novaContabilidade, foto) {
    let formData = this.montFormData(novaContabilidade);
    formData.append('Plano', novaContabilidade.Plano);
    formData.append('foto', foto);
    return this.http.post( API + 'contabilidade', formData);    
  }

  montFormData(values){
    let formData = new FormData();
    formData.append('Cnpj', values.Cnpj);
    formData.append('Nome_Fantasia', values.Nome_Fantasia);
    formData.append('Razao_Social', values.Razao_Social);
    formData.append('id', values.id);
    formData.append('status', values.status);
    return formData;    
  }
}
