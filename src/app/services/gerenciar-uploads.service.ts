import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment'

import { AppSettings } from "../app.settings";
import { Service } from './service'
import { Observable } from 'rxjs';
import { Table } from '../models/table';

const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class GerenciarUploadsService extends Service{

    constructor(private http: HttpClient ) {
        super();
    }

    getUploads(idContabilidade): Observable<Table[]>{
        return this.http.get<Table[]>(API + 'um-registro-empresa-por-contabilidade/' + idContabilidade);
    }

   
}
