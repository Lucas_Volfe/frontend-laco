import { TestBed } from '@angular/core/testing';

import { CalculosBpService } from './calculos-bp.service';

describe('CalculosBpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculosBpService = TestBed.get(CalculosBpService);
    expect(service).toBeTruthy();
  });
});
