import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Service } from './service';
import { environment } from './../../environments/environment'
import { BdUpload } from '../models/bd-upload';
import { Table } from '../models/table';

const API = environment.ApiUrl;

@Injectable({
  providedIn: 'root'
})
export class ChartsService extends Service{

  constructor(private http: HttpClient ) {
      super();
  }

  getConjuntoDadosBd(idEmpresa): Observable<BdUpload[]>{      
    return this.http.get<BdUpload[]>( API + 'todos-valores-charts/' + idEmpresa);
  }

  getEmpresasPorContabilidade(idContabilidade): Observable<Table[]>{
    return this.http.get<Table[]>(API + 'um-registro-upload-por-empresa/' + idContabilidade);
  }
 
}
