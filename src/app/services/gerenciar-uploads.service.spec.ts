import { TestBed } from '@angular/core/testing';

import { GerenciarUploadsService } from './gerenciar-uploads.service';

describe('GerenciarUploadsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GerenciarUploadsService = TestBed.get(GerenciarUploadsService);
    expect(service).toBeTruthy();
  });
});
