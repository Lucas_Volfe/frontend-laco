import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { environment } from '../../environments/environment';

const API = environment.ApiUrl;


@Injectable({
  providedIn: 'root'
})
export class AuthService{


  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  authenticate(userName: string, password: string) {   
    return this.http.post( 
        API + 'oauth', 
        {username: userName, password, "grant_type": "password"}, 
        {headers: new HttpHeaders({ 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Basic dGVzdGNsaWVudDp0ZXN0cGFzcw=='
      })})
      .pipe( tap( res => {
        this.userService.setToken(res['access_token']);
      }) )
  } 
}
