import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from './token.service';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate{

    public user$: Observable<User>;
    public user;

    constructor(
    private router: Router,
    private userService: UserService,
    private tokenService: TokenService
    ) { this.user$ = userService.getUser(); }

    canActivate( 
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): boolean| Observable<boolean> | Promise<boolean>
    {     
        if( this.userService.isLogged() ){
            //this.getUser();
            this.user$.subscribe(data => {
                if (data){
                    if ( state.url == '/login' ){                        
                        if (data.Contabilidade != null){
                            this.router.navigate(['contabilidade'])
                        }else if (data.Empresa != null){
                            this.router.navigate(['empresa'])
                        } else{
                            this.router.navigate(['administracao'])
                        }

                        return false;
                    }    
                }
            })


            if ( state.url == '/login' ){
                if ( this.user ){
                    (this.user.Contabilidade != null) ?
                        this.router.navigate(['contabilidade']):
                        this.router.navigate(['empresa']);
                }

                return false;
            }    
              
        }else if (!this.userService.isLogged() && state.url != '/login') {
            this.router.navigate(['login']) 
        }
        return true;
    }

   

}