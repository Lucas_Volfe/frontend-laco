import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Service } from './service';


const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class UserService {

  private userSubject = new BehaviorSubject<User>(null);

  constructor(
    private tokenService: TokenService,
    private http: HttpClient,
    ) {
    this.tokenService.hasToken() && this.getIdentityAndSetExpires();
  }

  setToken(token: string) {
    this.tokenService.setToken(token);
    this.getIdentityAndSetExpires();
  }

  getUser() {
    return this.userSubject.asObservable();
  }
  
  getIdentityAndSetExpires(id = null) {
    if(id)
      id = '&id='+id;
    else
      id = '';
    this.http.get(
        API + 'identity?token=' + this.tokenService.getToken() + id

      ).subscribe( data => {
        const user = new User();
        let name;
        
        user.username = data['first_name'];   
        user.Empresa = data['empresa'];
        user.Contabilidade = data['contabilidade'];
        user.adminProfile = data['adminProfile'];
        user.foto = data['foto'];
        data['contabilidade'] ? 
          name = data['name_contabilidade'] : 
          data['empresa']?
            name = data['name_empresa']:
            name = '';

        user.nome_fantasia = name;
        this.userSubject.next(user);
        this.tokenService.setExpireDate(data['expires']['date']);
    })
  }

  logout() {
    this.tokenService.removeToken();
    this.userSubject.next(null);
  }

  isLogged() {
    return this.tokenService.hasToken();
  }
}