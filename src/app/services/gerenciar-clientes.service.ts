import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../environments/environment'
import { Service } from './service'
import { Table } from '../models/table';


const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class GerenciarClientesService extends Service{

    constructor(private http: HttpClient ) {
        super();
    }

    getEmpresasPorContabilidade(idContabilidade): Observable<Table[]>{
        return this.http.get<Table[]>(API + 'um-registro-empresa-por-contabilidade/' + idContabilidade);
    }

   
}