import { Injectable } from '@angular/core';
import { Service } from './service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

const API = environment.ApiUrl;
@Injectable({
  providedIn: 'root'
})
export class HistoricoUploadsBpService extends Service{

  constructor(
    private http: HttpClient,    
    ) {
      super();
  }

  todosBalancoPatrimonialEmpresa(idEmpresa): Observable<any[]>{
    return this.http.get<any[]>(API + 'balanco-patrimonial/' + idEmpresa);
  }

  checkDisponibilidadeBalancoPatrimonial(idEmpresa): Observable<any[]>{
    return this.http.get<any[]>(API + 'check-disponibilidade-balanco-patrimonial/' + idEmpresa);
  }

}