import { HttpHeaders } from '@angular/common/http';

export class Service {
    protected headers = {headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization':  'Bearer ' + localStorage.getItem("authToken")// 'Basic dGVzdGNsaWVudDp0ZXN0cGFzcw=='
    })};

}
