import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Service } from './service';
import { Observable } from 'rxjs';

const API = environment.ApiUrl;


@Injectable({
  providedIn: 'root'
})
export class DashboardsAdminService extends Service{

  constructor(private http: HttpClient ) {
    super();
  }

  getResults(): Observable<any[]>{ 
    return this.http.get<any[]>(API + 'dashboards-admin');
  }
}
