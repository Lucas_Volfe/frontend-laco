import { Injectable } from '@angular/core';
import { BdUpload } from '../models/bd-upload';

@Injectable({
  providedIn: 'root'
})
export class CalculosDreService {

  constructor() { }

  

  somaReceitasOperacionais(coluna: BdUpload){
    coluna.Receitas_Operacionais = 
    coluna.Receita_De_Vendas +
    coluna.Receita_De_Fretes_E_Entregas +
    coluna.Outras_Receitas_1 +
    coluna.Outras_Receitas_2;

    coluna
    debugger
    return coluna
  }

  somaDeducoesReceitaBruta(coluna: BdUpload){
    coluna.Deducoes_Da_Receita_Bruta = 
      coluna.Impostos_Sobre_Vendas +
      coluna.Comissoes_Sobre_Vendas +
      coluna.Descontos_Incondicionais +
      coluna.Devolucoes_De_Vendas +
      coluna.Outras_Deducoes_1+
      coluna.Outras_Deducoes_2;

    return coluna
  }

  somaReceitaLiquidaVenda(coluna: BdUpload){
    coluna.Receita_Liquida_De_Vendas = 
      coluna.Receitas_Operacionais - 
      coluna.Deducoes_Da_Receita_Bruta;

    return coluna
  }

  somaCustosOperacionais(coluna: BdUpload){
    coluna.Custos_Operacionais = 
      coluna.Custos_Operacionais + 
      coluna.Custo_Das_Mercadorias_Vendidas +
      coluna.Custo_Dos_Produtos_Vendidos +
      coluna.Custo_Dos_Servicos_Prestados+
      coluna.Outros_Custos;

    return coluna
  }

  somaLucroBruto(coluna: BdUpload){
    coluna.Lucro_Bruto = 
      coluna.Receita_Liquida_De_Vendas -
      coluna.Custos_Operacionais;

    return coluna
  }

  somaDespesasOperacionais(coluna: BdUpload){
    coluna.Despesas_Operacionais = 
      coluna.Despesas_Com_Vendas +
      coluna.Despesas_Administrativas +
      coluna.Despesas_Tributarias_Gerais +
      coluna.Outras_Despesas_1 +
      coluna.Outras_Despesas_2 +
      coluna.Outras_Despesas_3 +
      coluna.Outras_Despesas_4;

    return coluna
  }

  somaLucroPrejuizoOperacional(coluna: BdUpload){
    coluna.Lucro_Prejuizo_Operacional =
      coluna.Lucro_Bruto -
      coluna.Despesas_Operacionais;

    return coluna
  }

  somaReceitasDespesasFinanceiras(coluna: BdUpload){
    coluna.Receitas_E_Despesas_Financeiras =
      coluna.Receitas_E_Rendimentos_Financeiros -
      coluna.Despesas_Financeiras + 
      coluna.Outras_Receitas_Financeiras -
      coluna.Outras_Despesas_Financeiras;

    return coluna
  }

  somaOutrasReceitasDespesasNaoOperacionais(coluna: BdUpload){
    coluna.Outras_Receitas_E_Despesas_Nao_Operacionais =
      coluna.Outras_Receitas_Nao_Operacionais -
      coluna.Outras_Despesas_Nao_Operacionais;

    return coluna
  }

  somaLucroPrejuizoLiquido(coluna: BdUpload){
    coluna.Lucro_Prejuizo_Liquido =
      coluna.Lucro_Prejuizo_Operacional -
      coluna.Receitas_E_Despesas_Financeiras -
      coluna.Outras_Receitas_E_Despesas_Nao_Operacionais;

    return coluna
  }

  somaDesembolsoInvestimentosEmprestimos(coluna: BdUpload){
    coluna.Desembolso_Com_Investimentos_E_Emprestimos =
      coluna.Investimentos_Em_Imobilizado +
      coluna.Emprestimos_E_Dividas +
      coluna.Outros_Investimentos_E_Emprestimos;

    return coluna
  }

  somaLucroPrejuizoFinal(coluna: BdUpload){
    coluna.Lucro_Prejuizo_Final = 
      coluna.Lucro_Prejuizo_Liquido -
      coluna.Desembolso_Com_Investimentos_E_Emprestimos

    return coluna
  }


  somasCelulasFormulasExcel(coluna: BdUpload){        
    
    coluna.Receitas_Operacionais = 
      coluna.Receita_De_Vendas +
      coluna.Receita_De_Fretes_E_Entregas +
      coluna.Outras_Receitas_1 +
      coluna.Outras_Receitas_2;

    coluna.Deducoes_Da_Receita_Bruta = 
      coluna.Impostos_Sobre_Vendas +
      coluna.Comissoes_Sobre_Vendas +
      coluna.Descontos_Incondicionais +
      coluna.Devolucoes_De_Vendas +
      coluna.Outras_Deducoes_1+
      coluna.Outras_Deducoes_2;

    coluna.Receita_Liquida_De_Vendas = 
      coluna.Receitas_Operacionais - 
      coluna.Deducoes_Da_Receita_Bruta;

    coluna.Custos_Operacionais = 
      coluna.Custos_Operacionais + 
      coluna.Custo_Das_Mercadorias_Vendidas +
      coluna.Custo_Dos_Produtos_Vendidos +
      coluna.Custo_Dos_Servicos_Prestados+
      coluna.Outros_Custos;
    
    coluna.Lucro_Bruto = 
      coluna.Receita_Liquida_De_Vendas -
      coluna.Custos_Operacionais;

    coluna.Despesas_Operacionais = 
      coluna.Despesas_Com_Vendas +
      coluna.Despesas_Administrativas +
      coluna.Despesas_Tributarias_Gerais +
      coluna.Outras_Despesas_1 +
      coluna.Outras_Despesas_2 +
      coluna.Outras_Despesas_3 +
      coluna.Outras_Despesas_4;

    coluna.Lucro_Prejuizo_Operacional =
      coluna.Lucro_Bruto -
      coluna.Despesas_Operacionais;  
     
    coluna.Receitas_E_Despesas_Financeiras =
      coluna.Receitas_E_Rendimentos_Financeiros -
      coluna.Despesas_Financeiras + 
      coluna.Outras_Receitas_Financeiras -
      coluna.Outras_Despesas_Financeiras;

    coluna.Outras_Receitas_E_Despesas_Nao_Operacionais =
      coluna.Outras_Receitas_Nao_Operacionais -
      coluna.Outras_Despesas_Nao_Operacionais;

    coluna.Lucro_Prejuizo_Liquido =
      coluna.Lucro_Prejuizo_Operacional -
      coluna.Receitas_E_Despesas_Financeiras -
      coluna.Outras_Receitas_E_Despesas_Nao_Operacionais;

    coluna.Desembolso_Com_Investimentos_E_Emprestimos =
      coluna.Investimentos_Em_Imobilizado +
      coluna.Emprestimos_E_Dividas +
      coluna.Outros_Investimentos_E_Emprestimos;

    coluna.Lucro_Prejuizo_Final = 
      coluna.Lucro_Prejuizo_Liquido -
      coluna.Desembolso_Com_Investimentos_E_Emprestimos

    return coluna
  }


}
