interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}



export const navItemsEmpresa: NavData[] = [
  {
    name: 'Dashboards',
    url: '/empresa',
    icon: 'icon_gerenciar_dashboard container_icon',
    class: 'mt-5'
  },
  {
    name: 'Histórico Uploads',
    url: '/empresa/historico-uploads',
    icon: 'icon_gerenciar_clientes container_icon'
  }
];
