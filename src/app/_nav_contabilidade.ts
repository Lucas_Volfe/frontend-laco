interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}



export const navItemsContabilidade: NavData[] = [
  /*{
    name: 'Gerenciar Dashboard',
    url: '/contabilidade/gerenciar-dashboards',
    icon: 'icon_gerenciar_dashboard container_icon',
    class: 'mt-5'
  },*/
  {
    name: 'Gerenciar Clientes',
    url: '/contabilidade/gerenciar-clientes',
    icon: 'icon_gerenciar_clientes container_icon',
    class: 'mt-5'
  },
  {
    name: 'Novo Cliente',
    url: '/contabilidade/novo-cliente',
    icon: 'icon_novo_cliente container_icon'
  },
  {
    name: 'Gerenciar Uploads',
    url: '/contabilidade/gerenciar-uploads',
    icon: 'icon_gerenciar_uploads container_icon',
  },/*
  {
    name: 'Upar Relatório',
    url: '/contabilidade/upar-relatorio',
    icon: 'icon_upar_relatorio container_icon',
  },*/
  {
    title: true,
    name: 'Precisa de Ajuda?',
    class: 'precisa_ajuda'
  },
  {
    name: 'Treinamentos',
    url: '/contabilidade/treinamentos',
    class: 'btn_treinamentos'
  },
  {
    name: 'Suporte Online',
    url: '/contabilidade/treinamentos',
    class: 'btn_suporte'
  }
];
