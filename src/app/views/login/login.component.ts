import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';    
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit { 
  public loading = false;

  loginForm: FormGroup;
  @ViewChild('userNameInput', {static: false}) userNameInput: ElementRef<HTMLInputElement>;  
  public user$: Observable<User>;

  constructor(
    private _toastr: ToastrService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ){ this.user$ = userService.getUser(); }

  login() {
    this.loading = true;
    const userName = this.loginForm.get('userName').value;
    const password = this.loginForm.get('password').value;
    this.authService.authenticate(userName, password).subscribe( data=> {
      this.redirecionaPorPerfil();
      //this.router.navigate(['user', userName]) //Passa variavel nao necessitando concatenar
      //console.log(data)
    }, err => {
      console.log(err);
      this.loading = false;
      this._toastr.error('Usuário ou senha errada!', 'Oops!');
      this.loginForm.reset();
      this.userNameInput.nativeElement.focus();
    })
  }

  redirecionaPorPerfil(){

    this.user$.subscribe( data => {
      if(data){
        (data.Contabilidade != null) ? this.router.navigateByUrl('contabilidade') : false;
        (data.Empresa != null) ? this.router.navigateByUrl('empresa') : false;
        (data.Empresa == null && data.Contabilidade == null) ? this.router.navigateByUrl('administracao') : false;
      }
    });

    //this.router.navigateByUrl('contabilidade');
  }

  ngOnInit(): void{
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });    
  }

}
