import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Table } from '../../../models/table';
import { GerenciarClientesService } from '../../../services/gerenciar-clientes.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-gerenciar-clientes',
  templateUrl: './gerenciar-clientes.component.html',
  styleUrls: ['./gerenciar-clientes.component.scss']
})
export class GerenciarClientesComponent implements OnInit, OnDestroy {
  public user$: Observable<User>;
  public clientes: Table[];
  public dtTrigger: Subject<any> = new Subject()
  public tableTemDados = false;
  public loading = true;


  constructor(
    private clientesService : GerenciarClientesService,
    private userService: UserService,
    private router: Router
  ){ this.user$ = userService.getUser() }  


  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    this.dtOptions.language = {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
    };
    
    this.user$.subscribe( data => { 
      if (data && (data.Contabilidade != null)) {
        this.clientesService.getEmpresasPorContabilidade((data.Contabilidade)).subscribe( data => {
          if ( data[0] != null ) {   
            this.tableTemDados = true;
            this.clientes = data;
            this.loading = false;
            
            this.dtTrigger.next();
          }
        }, error => {
          this.loading = false;
          this.dtTrigger.next();
          console.log(error);
        });
      } 
    });
    
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  

}
