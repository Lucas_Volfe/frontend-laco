import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GerenciarDashboardsComponent } from './gerenciar-dashboards/gerenciar-dashboards.component';
import { VisualizarDashboardsComponent } from './visualizar-dashboards/visualizar-dashboards.component';
import { IndexComponent } from './index/index.component';
import { GerenciarClientesComponent } from './gerenciar-clientes/gerenciar-clientes.component';
import { GerenciarUploadsComponent } from './gerenciar-uploads/gerenciar-uploads.component';
import { UparRelatorioComponent } from './upar-relatorio/upar-relatorio.component';
import { TreinamentoComponent } from './treinamento/treinamento.component';
import { HistoricoUploadsComponent } from './historico-uploads/historico-uploads.component';
import { MinhaContabilidadeComponent } from './minha-contabilidade/minha-contabilidade.component';
import { FormularioNovoUsuarioComponent } from '../cadastro/formulario-novo-usuario/formulario-novo-usuario.component';
import { FormularioNovaEmpresaComponent } from '../cadastro/formulario-nova-empresa/formulario-nova-empresa.component';
import { BalancoPatrimonialComponent } from './upar-relatorio/balanco-patrimonial/balanco-patrimonial.component';
import { VisualizarDashboardsResolver } from './visualizar-dashboards/visualizar-dashboards.resolver';
import { TableDREComponent } from './gerenciar-uploads/table-dre/table-dre.component';
import { TableBPComponent } from './gerenciar-uploads/table-bp/table-bp';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Contabilidade'
    },
    children:[
      {
        path: '',
        component: IndexComponent,
        data: {
          title: 'Contabilidade'
        }
      },
      {
        path: 'gerenciar-dashboards',
        component: GerenciarDashboardsComponent,
        data:{
          title: 'Gerenciar Dashboards'
        }
      },
      {
        path: 'gerenciar-dashboards/visualizar',
        component: VisualizarDashboardsComponent,
        data:{
          title: 'Visualizar Dashboards'
        }
      },
      {
        path: 'gerenciar-dashboards/visualizar/:id',
        component: VisualizarDashboardsComponent,
        data:{
          title: 'Visualizar Dashboards'
        },
        resolve:{ dados: VisualizarDashboardsResolver}
      },
      {
        path: 'novo-cliente',
        component: FormularioNovaEmpresaComponent,
        data: {
          title: 'Novo Cliente'
        }
      },
      {
        path: 'novo-cliente/:id',
        component: FormularioNovaEmpresaComponent,
        data: {
          title: 'Novo Cliente'
        }
      },
      {
        path: 'novo-usuario',
        component: FormularioNovoUsuarioComponent,
        data: {
          title: 'Novo Usuário'
        }
      },
      {
        path: 'gerenciar-clientes',
        component: GerenciarClientesComponent,
        data: {
          title: 'Gerenciar Clientes'
        }
      },
      {
        path: 'gerenciar-uploads',
        component: GerenciarUploadsComponent,
        data: {
          title: 'Gerenciar Uploads'
        }
      }
      ,
      {
        path: 'historico-uploads/:id',
        component: HistoricoUploadsComponent,
        data: {
          title: 'Histórico Uploads'
        }
      },                  
      {
        path: 'upar-relatorio',
        component: UparRelatorioComponent,
        data: {
          title: 'Upar Relatório'
        }
      },
      {
        path: 'balanco-patrimonial/:id',
        component: BalancoPatrimonialComponent,
        data: {
          title: 'Balanço Patrimonial'
        }
      },
      {
        path: 'treinamentos',
        component: TreinamentoComponent,
        data: {
          title: 'Treinamentos'
        }
      },           
      {
        path: 'minha-contabilidade',
        component: MinhaContabilidadeComponent,
        data: {
          title: 'Minha Contabilidade'
        }
      },           
      {
        path: 'table-dre/:id',
        component: TableDREComponent,
        data: {
          title: 'Table DRE'
        }
      },
      {
        path: 'table-bp/:id',
        component: TableBPComponent,
        data: {
          title: 'Table BP'
        }
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContabilidadeRoutingModule {}
