import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoUploadsComponent } from './historico-uploads.component';

describe('HistoricoUploadsComponent', () => {
  let component: HistoricoUploadsComponent;
  let fixture: ComponentFixture<HistoricoUploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoUploadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoUploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
