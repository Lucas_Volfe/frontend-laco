import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { Observable } from 'rxjs';
import { User } from '../../../models/user';
import { UploadDadosService } from '../../../services/upload-dados.service';

@Injectable()
export class HistoricoUploadsResolver implements Resolve<any>{

    public user$: Observable<User>;
    public historicos;

    constructor(
        private userService: UserService,
        private _uploadDadosService: UploadDadosService
    )
    { this.user$ = userService.getUser(); }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let id = route.params['id']         
        if(id){
            this._uploadDadosService.getHistoricoUploads(id).subscribe( data => {
                return data
            })
        }else{
            this.user$.subscribe( data => {
                if ( data ) {
                    this._uploadDadosService.getHistoricoUploads(data.Empresa).subscribe( result => {
                        return result
                    })
                } 
            });
        }
    }

}