import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HistoricoUploads } from '../../../models/historico-uploads';
import { UploadDadosService } from '../../../services/upload-dados.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-historico-uploads',
  templateUrl: './historico-uploads.component.html',
  styleUrls: ['./historico-uploads.component.scss']
})
export class HistoricoUploadsComponent implements OnInit {
 
  public dtTrigger: Subject<any> = new Subject()
  public dtOptions: DataTables.Settings = {};
  public loading = false;
  public uploads:HistoricoUploads[] = [];
  public user$: Observable<User>;

  constructor(
    private _uploadDadosService: UploadDadosService,
    private userService: UserService,
    private route: ActivatedRoute
    ) { this.user$ = userService.getUser(); }


  getHistorico(empresa_id){
    this._uploadDadosService.getHistoricoUploads(empresa_id).subscribe( data => {
      if(data){
        this.uploads = data;
        this.loading = false;
        this.dtTrigger.next();
      }
    }, err => {
      this.dtTrigger.next()
      this.loading = false;    
    })
  }

  ngOnInit() {

    this.user$.subscribe( data => {
      if (data ){
        ( data.Empresa != null ) ? this.getHistorico(data.Empresa): 
          this.getHistorico(this.route.snapshot.paramMap.get('id')) 
        
      }
    });

    // this.route.data.subscribe(data => {     
      //   this.uploads = data.historico;
      //   this.dtTrigger.next();
      // })
        
    this.dtOptions.language = {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json",
    }
  }


}
