import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarUploadsComponent } from './gerenciar-uploads.component';

describe('GerenciarUploadsComponent', () => {
  let component: GerenciarUploadsComponent;
  let fixture: ComponentFixture<GerenciarUploadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarUploadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarUploadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
