import { Component, OnInit } from '@angular/core';
import { GerenciarUploadsService } from '../../../services/gerenciar-uploads.service';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { Subject, Observable } from 'rxjs';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-gerenciar-uploads',
  templateUrl: './gerenciar-uploads.component.html',
  styleUrls: ['./gerenciar-uploads.component.scss']
})


export class GerenciarUploadsComponent implements OnInit {

  public dtTrigger: Subject<any> = new Subject();
  public uploads: Object[];
  public tableTemDados = false;
  public user$: Observable<User>;
  public loading = true;  
  public dtOptions: DataTables.Settings = {};

  
  constructor( 
    private userService: UserService,
    private uploadsService : GerenciarUploadsService
   ) { this.user$ = userService.getUser() }
  
  
  ngOnInit() {

    this.user$.subscribe( data => {
      if (data && (data.Contabilidade != null)) {
        this.uploadsService.getUploads((data.Contabilidade)).subscribe( data => {
          this.loading = false;
          if ( data[0] != null ) {            
            this.tableTemDados = true;
            this.uploads = data;
            console.log(this.uploads);
            this.dtTrigger.next();
          }
        }, error => {
          console.log(error);
          this.dtTrigger.next();
          this.loading = false;

        });
      } 
    });
    
    /*this.uploadsService.getUploads().subscribe( data => {
      this.uploads = data;
      this.dtTrigger.next();

    }); */     
      
    this.dtOptions.language = {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
    }

  }



}
