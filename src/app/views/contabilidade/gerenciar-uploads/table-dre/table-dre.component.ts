import { Component, OnInit } from '@angular/core';
import { HistoricoUploadsDreService } from '../../../../services/historico-uploads-dre.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BdUpload } from '../../../../models/bd-upload';
import { CalculosDreService } from '../../../../services/calculos-dre.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadDadosService } from '../../../../services/upload-dados.service';




@Component({
  selector: 'app-table-dre',
  templateUrl: './table-dre.component.html',
  styleUrls: ['./table-dre.component.scss']
})


export class TableDREComponent implements OnInit {

  public registerForm: FormGroup;
  public monthsRegistered;
  public fullHistoric;
  public selected;
  public changed = false;

  constructor( 
    private historicoUploadService: HistoricoUploadsDreService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router:Router,
    private toastr: ToastrService,
    private calculosService: CalculosDreService,
    private uploadService: UploadDadosService
   ) {  }

  constroiArrayMeses(data){
    //Monta array de meses para o Ng-Select 
    let meses = [];
    data.forEach((coluna, i) => {
      meses.push({
        id: i,
        mes: coluna.Mes + '/' + coluna.Ano
      });
    });
    this.monthsRegistered = meses;
    this.selected = meses[0].id
  }

  selectMounth(i){
    this.mountFormGroup(this.fullHistoric[i])
  }

  open(content) {
    (this.changed)?
      this.modalService.open(content):
      this.save()
  }

  

  save(){
    this.modalService.dismissAll()
    this.uploadService.create([this.registerForm.getRawValue()]).subscribe(()=>{
      this.toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
      this.router.navigate(['']);
    }, err=>{
      this.toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
      console.log(err)
    })
  }


  
  ngOnInit() {
    this.mountFormGroup(null)   
    this.historicoUploadService.getHistoricoUpload(this.route.snapshot.paramMap.get('id')).subscribe(data => {
      this.constroiArrayMeses(data)
      this.mountFormGroup(data[0])
      this.fullHistoric = data
    }) 

    
    

  }



  mountFormGroup(data){
    if(!data){
      data = new BdUpload()
    }    
    this.registerForm = this.formBuilder.group({
      id: data.id,
      Mes: data.Mes,
      Ano: data.Ano,
      Data_Upload: new Date(),
      Empresa: Number(this.route.snapshot.paramMap.get('id')),
      
      Receitas_Operacionais: data.Receitas_Operacionais,
      Receita_De_Vendas: data.Receita_De_Vendas,
      Receita_De_Fretes_E_Entregas: data.Receita_De_Fretes_E_Entregas,
      Outras_Receitas_1: data.Outras_Receitas_1,
      Outras_Receitas_2: data.Outras_Receitas_2,

      Deducoes_Da_Receita_Bruta: data.Deducoes_Da_Receita_Bruta,
      Impostos_Sobre_Vendas: data.Impostos_Sobre_Vendas,
      Comissoes_Sobre_Vendas: data.Comissoes_Sobre_Vendas,
      Descontos_Incondicionais: data.Descontos_Incondicionais,
      Devolucoes_De_Vendas: data.Devolucoes_De_Vendas,
      Outras_Deducoes_1: data.Outras_Deducoes_1,
      Outras_Deducoes_2: data.Outras_Deducoes_2,

      Receita_Liquida_De_Vendas: data.Receita_Liquida_De_Vendas,

      Custos_Operacionais: data.Custos_Operacionais,
      Custo_Das_Mercadorias_Vendidas: data.Custo_Das_Mercadorias_Vendidas,
      Custo_Dos_Produtos_Vendidos: data.Custo_Dos_Produtos_Vendidos,
      Custo_Dos_Servicos_Prestados: data.Custo_Dos_Servicos_Prestados,
      Outros_Custos: data.Outros_Custos,

      Lucro_Bruto: data.Lucro_Bruto,

      Despesas_Operacionais: data.Despesas_Operacionais,
      Despesas_Com_Vendas: data.Despesas_Com_Vendas,
      Despesas_Administrativas: data.Despesas_Administrativas,
      Despesas_Tributarias_Gerais: data.Despesas_Tributarias_Gerais,
      Outras_Despesas_1: data.Outras_Despesas_1,
      Outras_Despesas_2: data.Outras_Despesas_2,
      Outras_Despesas_3: data.Outras_Despesas_3,
      Outras_Despesas_4: data.Outras_Despesas_4,

      Lucro_Prejuizo_Operacional: data.Lucro_Prejuizo_Operacional,

      Receitas_E_Despesas_Financeiras: data.Receitas_E_Despesas_Financeiras,
      Receitas_E_Rendimentos_Financeiros: data.Receitas_E_Rendimentos_Financeiros,
      Despesas_Financeiras: data.Despesas_Financeiras,
      Outras_Receitas_Financeiras: data.Outras_Receitas_Financeiras,
      Outras_Despesas_Financeiras: data.Outras_Despesas_Financeiras,

      Outras_Receitas_E_Despesas_Nao_Operacionais: data.Outras_Receitas_E_Despesas_Nao_Operacionais,
      Outras_Receitas_Nao_Operacionais: data.Outras_Receitas_Nao_Operacionais,
      Outras_Despesas_Nao_Operacionais: data.Outras_Despesas_Nao_Operacionais,

      Lucro_Prejuizo_Liquido: data.Lucro_Prejuizo_Liquido,

      Desembolso_Com_Investimentos_E_Emprestimos: data.Desembolso_Com_Investimentos_E_Emprestimos,
      Investimentos_Em_Imobilizado: data.Investimentos_Em_Imobilizado,
      Emprestimos_E_Dividas: data.Emprestimos_E_Dividas,
      Outros_Investimentos_E_Emprestimos: data.Outros_Investimentos_E_Emprestimos,

      Lucro_Prejuizo_Final: data.Lucro_Prejuizo_Final,
    });
    
    this.registerForm.valueChanges.subscribe(data=>{
      this.changed = true;  
    })
  }



}
