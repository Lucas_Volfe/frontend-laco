import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HistoricoUploadsBpService } from '../../../../services/historico-uploads-bp.service';
import { BalancoPatrimonial } from '../../../../models/balanco-patrimonial';


@Component({
  selector: 'app-table-bp',
  templateUrl: './table-bp.component.html',
  // styleUrls: ['./table-bp.component.scss']
})


export class TableBPComponent implements OnInit {
  
  public monthsRegistered;
  public dados;
  public selected;

  constructor( 
    private historicoUploadService: HistoricoUploadsBpService,
    private route: ActivatedRoute
   ) {  }

  constroiArrayMeses(data){
    //Monta array de meses para o Ng-Select 
    let meses = [];
    
    data.forEach((coluna, i) => {
      meses.push({
          id: coluna.id,
          mes: coluna.Mes + '/' + coluna.Ano
        });
    });
    this.monthsRegistered = meses;
    this.selected = meses[0].id
  }


  
  
  
  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.historicoUploadService.checkDisponibilidadeBalancoPatrimonial(id).subscribe(data => {
      this.constroiArrayMeses(data)
    })   
    this.historicoUploadService.todosBalancoPatrimonialEmpresa(id).subscribe(data =>{
      this.dados = data;
    })

  }



}
