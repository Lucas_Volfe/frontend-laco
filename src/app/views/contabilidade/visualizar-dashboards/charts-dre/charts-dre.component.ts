
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subject, observable, Observable } from 'rxjs';

import { ChartsService } from '../../../../services/charts.service';
import { ConjuntoObjtSerie } from '../../../../models/charts/conjuntoObjtSerie';
import { ObjtSerie } from '../../../../models/charts/objtSerie';
import { BdUpload } from '../../../../models/bd-upload';
import { MesesNumero } from '../../../../models/charts/mesesNumero';
import { User } from '../../../../models/user';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute,} from '@angular/router';



class LabelSelects{
  ano: number;
  id: number;
  value: number;
  type: string;
  label: string;
}


@Component({
  selector: 'app-charts-dre',
  templateUrl: './charts-dre.component.html',
  styleUrls: ['./charts-dre.scss'],
})
export class ChartsDREComponent implements OnInit {

  public dtTrigger: Subject<any> = new Subject()
  public atributos = Object.getOwnPropertyNames( new BdUpload() ); 
  public periodoTrimestreSelect = 0;
  public periodoSemestreSelect = 0;
  public periodoMensalSelect = 0;

  public periodoVisualizacao = 1;
  public lastDataUpdate;


  public user$: Observable<User>;

  @Input('dados')
  public dados = null;

  public empresas = null;
  public trimestres = null;
  public semestres = null;

  /*  VALORES PARA OS CHARTS  */
  public valorChart_LucroLiquidoMensal = [];
  public todosValoresChartLucroLiquidoMensal = [];

  public valorChart_LucroLiquidoAcumulado = [];
  
  public valorChart_GastosTotais = [];
  public todosValoresGastosTotais = [];

  public valorChart_DeducaoDaReceita = [];
  public todosValoresDeducaoDaReceita = [];
  
  public valorChart_ReceitasOperacionais = [];
  public valoresAuxiliares_ReceitasOperacionais;

  public valorChart_CustosOperacionais = [];
  public valoresAuxiliares_CustosOperacionais;

  public valorChart_DespesasOperacionais = [];
  public valoresAuxiliares_DespesasOperacionais;
  
  public valorChart_PesoCustoDespesa;  

  public valorChart_PontoEquilibrio;  

  public valorChart_ResultadoApurado;  

  public valorChart_AnaliseResultadoLiquido;  
  public todosValoresAnaliseResultadoLiquido;



  

  /* VARIAVEIS DENTRO DOS CHARTS */
  public receitasOperacionais_VariacaoMes;
  public receitasOperacionais_MediaAno;
  public receitasOperacionais_AcumuladoAno;

  public custosOperacionais_VariacaoMes;
  public custosOperacionais_MediaAno;
  public custosOperacionais_AcumuladoAno;

  public despesasOperacionais_VariacaoMes;
  public despesasOperacionais_MediaAno;
  public despesasOperacionais_AcumuladoAno;

  public pontoEquilibrioMedio;


  /* MESES SELECT*/
  public mesesCadastrados = []
  public mesesCadastradosSelect = {id: 0, mes: ''}
  
  constructor(
    private userService: UserService,
    private _route: ActivatedRoute,

  ) { this.user$ = userService.getUser(); }

 


  
  mudarMesAnaliseGrafico(value, grafico){

    this.valorChart_GastosTotais = this.todosValoresGastosTotais[value]
    this.valorChart_LucroLiquidoMensal = this.todosValoresChartLucroLiquidoMensal[value]
    this.valorChart_DeducaoDaReceita = this.todosValoresDeducaoDaReceita[value]

    // if ( grafico == 'LucroLiquidoMensal' ){
    //   this.valorChart_LucroLiquidoMensal = this.todosValoresChartLucroLiquidoMensal[value]
    // }
    // else if ( grafico == 'gastosTotais' ){
    //   this.valorChart_GastosTotais = this.todosValoresGastosTotais[value]
    // }
    // else if ( grafico == 'DeducaoDaReceita' ){
    //   this.valorChart_DeducaoDaReceita = this.todosValoresDeducaoDaReceita[value]
    // }
  }

  mudarTrimestreAnaliseGrafico(id){
    let ano = this.trimestres[id].ano;
    let trimestre = this.trimestres[id].value; 
    this.selectEmpresa(this.dados.trimestres[ano][trimestre]);
  }

  mudarSemestreAnaliseGrafico(id){
    let ano = this.semestres[id].ano;
    let semestre = this.semestres[id].value;    

    this.selectEmpresa(this.dados.semestres[ano][semestre]);
  }

  selectEmpresa(data){   
    
    this.valorChart_ReceitasOperacionais = (data!=null) ?  data['ReceitasOperacionais']:[]; 
    this.valorChart_CustosOperacionais = (data!=null) ? data['CustosOperacionais']:[]; 
    this.valorChart_DespesasOperacionais = (data!=null) ? data['DespesasOperacionais']:[]; 
    this.valorChart_PesoCustoDespesa = (data!=null) ? data['PesoCustoDespesa']:[]; 
    this.valorChart_PontoEquilibrio = (data!=null) ? data['PontoEquilibrio']:[]; 
    this.valorChart_ResultadoApurado = (data!=null) ? data['ResultadoApurado']:[]; 

    
    if(data!=null){
      
      this.lastDataUpdate = data;
      if(data.meses){
        if(data.meses.length > 12) this.view400 = [600,200];
        else if(data.meses.length > 30) this.view400 = [800,200];
      }
      
      //  NgSelect meses
      this.mesesCadastrados = data['meses'];
  
      //  Lucro Líquido Mensal
      this.valorChart_LucroLiquidoMensal = data['LucroLiquidoMensal'][0];
      this.todosValoresChartLucroLiquidoMensal = data['LucroLiquidoMensal'];
  
      //  Lucro Líquido Acumulado
      this.valorChart_LucroLiquidoAcumulado = data['LucroLiquidoAcumulado']; 
  
      //  Gastos Totais
      this.valorChart_GastosTotais = data['GastosTotais'][0];    
      this.todosValoresGastosTotais = data['GastosTotais'];
      
      //  Dedução da Receita
      this.valorChart_DeducaoDaReceita = data['DeducaoDaReceita'][0];    
      this.todosValoresDeducaoDaReceita = data['DeducaoDaReceita'];

      this.valorChart_AnaliseResultadoLiquido = data['AnaliseResultadoLiquido'];
      // this.valorChart_AnaliseResultadoLiquido = (data['AnaliseResultadoLiquido'][0]!=undefined) ? data['AnaliseResultadoLiquido'][0] : data['AnaliseResultadoLiquido'];
      this.todosValoresAnaliseResultadoLiquido = data['AnaliseResultadoLiquido'];

      //Se não imprimir o valor referente ao mes, pode ser removido
      let ref_RO = data['ReceitasOperacionais'][0];
      let ref_CO = data['CustosOperacionais'][0];
      let ref_DO = data['DespesasOperacionais'][0];
  
      this.onSelectReceitasOperacionais(ref_RO);
      this.onSelectCustosOperacionais(ref_CO);
      this.onSelectDespesasOperacionais(ref_DO);
      this.onSelectPontoEquilibrioMedio();
    }

    
    
  }

  togglePeriodo(periodo){
    this.periodoVisualizacao = periodo
    
    if (periodo == 1){
      this.selectEmpresa(this.dados.mensal);
      this.periodoMensalSelect = 0;
    }
    else if (periodo == 3){
      this.mudarTrimestreAnaliseGrafico(this.periodoTrimestreSelect);
      // this.periodoTrimestreSelect = 0;
    }
    else{
      this.mudarSemestreAnaliseGrafico(this.periodoSemestreSelect);
      // this.periodoSemestreSelect = 0;
    }
  }

  montarTrimestresSemestres(dados){

    let trimestres=[];
    let semestres=[];
    let id = 0;
    for(let trimestre in dados.trimestres ){
      for(let val in dados.trimestres[trimestre]){        
        let label = new LabelSelects();
        label.ano = Number(trimestre);
        label.value = Number(val);
        label.type = 'trimestres';
        label.id = id;
        label.label = val+' Trimestre de '+trimestre;
        trimestres.push(label)
        id++;   
      }
    }
    this.trimestres = trimestres;
    id = 0;
    for(let semestre in dados.semestres ){
      for(let val in dados.semestres[semestre]){        
        let label = new LabelSelects();
        label.ano = Number(semestre);
        label.value = Number(val);
        label.type = 'semestres';
        label.id = id;
        label.label = val+' Semestre de '+semestre;
        semestres.push(label)
        id++;   
      }
    }
    this.semestres = semestres;
  }

  ngOnInit() {    
    if(this.dados != false) {
      this.selectEmpresa(this.dados.mensal);
      this.montarTrimestresSemestres(this.dados)
    }
  }

  onSelectReceitasOperacionais(event){  
    let mediaAno = 0;
    let acumuladoAno = 0;
    this.valorChart_ReceitasOperacionais.forEach(element => {
      if(element.name == event.name){
        this.receitasOperacionais_VariacaoMes = Number(element.value); 
      }      
      mediaAno += Number(element.value);
      acumuladoAno += Number(element.value);      
    });
    this.receitasOperacionais_MediaAno = (mediaAno/this.valorChart_ReceitasOperacionais.length).toFixed(2);    
    this.receitasOperacionais_AcumuladoAno = acumuladoAno.toFixed(2);
  }

  onSelectCustosOperacionais(event){  
    let mediaAno = 0;
    let acumuladoAno = 0;
    this.valorChart_CustosOperacionais.forEach(element => {
      if(element.name == event.name){
        this.custosOperacionais_VariacaoMes = Number(element.value); 
      }      
      mediaAno += Number(element.value);
      acumuladoAno += Number(element.value);      
    });
    this.custosOperacionais_MediaAno = (mediaAno/this.valorChart_CustosOperacionais.length).toFixed(2);    
    this.custosOperacionais_AcumuladoAno = acumuladoAno.toFixed(2);
  }

  onSelectDespesasOperacionais(event){  
    let mediaAno = 0;
    let acumuladoAno = 0;
    this.valorChart_DespesasOperacionais.forEach(element => {
      if(element.name == event.name){
        this.despesasOperacionais_VariacaoMes = Number(element.value); 
      }      
      mediaAno += Number(element.value);
      acumuladoAno += Number(element.value);      
    });
    this.despesasOperacionais_MediaAno = (mediaAno/this.valorChart_DespesasOperacionais.length).toFixed(2);    
    this.despesasOperacionais_AcumuladoAno = acumuladoAno.toFixed(2);
  }

  onSelectPontoEquilibrioMedio(){  
    let acumulado = 0;
    this.valorChart_PontoEquilibrio[0]['series'].forEach(element => {
      acumulado += element.value;      
    });
    this.pontoEquilibrioMedio = (acumulado/this.valorChart_PontoEquilibrio.length).toFixed(2);
  }

  changeTab(idTab){
    this.selectEmpresa(null)
    
    setTimeout(() => {
      this.selectEmpresa(this.lastDataUpdate)

    }, 1);
  }


// options
view200: any[] = [200,200];
view400: any[] = [400,200];
showXAxis = true;
showYAxis = true;
gradient = false;
showLegend = true;
showXAxisLabel = true;
xAxisLabel = 'xAxisLabel';
showYAxisLabel = true;
yAxisLabel = 'yAxisLabel';
timeline = true;
label = "Label";
animations = true
showLabels = true;
explodeSlices = false;
doughnut = false;
colorScheme = {
  domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#6b00e9']
};
showDataLabel = true;

  
  legendTitle = 'Legend';
  legendPosition = 'right';

  showGridLines = true;
  innerPadding = '10%';


  showRightYAxisLabel: boolean = true;
  yAxisLabelRight: string = 'Utilization';

  lineChartSeries = [    
      {
      name: 'Computers',
      series: [
      {
        value: 0,
        name: 'USA',
  
      },
      {
        value: 4,
        name: 'United Kingdom'
      },
      {
        value: 20,
        name: 'France'
      },
      {
        value: 30,
        name: 'Japan'
      },
      {
        value: 35,
        name: 'China'
      }
      ]
    }
  ]

  barChart = [
    {
      name: 'USA',
      value: 50000
    },
    {
      name: 'United Kingdom',
      value: -30000
    },
    {
      name: 'France',
      value: 10000
    },
    {
      name: 'Japan',
      value: 5000
    },
    {
      name: 'China',
      value: 500
    }
  ]

  neg2 = [
    {
      "name": "Germany",
      "series": [
        {
          "name": "2010",
          "value": 20
        },
        {
          "name": "2011",
          "value": -30
        }
      ]
    },
    {
      "name": "Germany",
      "series": [
        {
          "name": "2010",
          "value": 5
        },
        {
          "name": "2011",
          "value": -2
        },
        {
          "name": "2012",
          "value": 3
        },
        {
          "name": "2013",
          "value": 7
        }
      ]
    },
  ]

  neg = [
    {
      "name": "jan-18",
      "series": [
        {
          "name": "receitas operacionais",
          "value": 398.55
        },
        {
          "name": "gastos totais",
          "value": -361
        }
      ]
    },  
    {
      "name": "fev-18",
      "series": [
        {
          "name": "receitas operacionais",
          "value": 413
        },
        {
          "name": "gastos totais",
          "value": -340
        }
      ]
    },  
    {
      "name": "mar-18",
      "series": [
        {
          "name": "receitas operacionais",
          "value": 380
        },
        {
          "name": "gastos totais",
          "value": -356
        }
      ]
    },  
    {
      "name": "abr-18",
      "series": [
        {
          "name": "receitas operacionais",
          "value": 400
        },
        {
          "name": "gastos totais",
          "value": -340
        }
      ]
    }
  ]



}

