import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarDashboardsComponent } from './visualizar-dashboards.component';

describe('VisualizarDashboardsComponent', () => {
  let component: VisualizarDashboardsComponent;
  let fixture: ComponentFixture<VisualizarDashboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarDashboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarDashboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
