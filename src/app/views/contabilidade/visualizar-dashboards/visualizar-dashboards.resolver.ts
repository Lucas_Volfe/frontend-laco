import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ChartsService } from '../../../services/charts.service';
import { UserService } from '../../../services/user.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../../../models/user';

@Injectable()
export class VisualizarDashboardsResolver implements Resolve<any>{
    

    public user$: Observable<User>;
    public dados;
    public idEmpresa;

    constructor(
        private userService: UserService,
        private chartService:ChartsService
    )
    { 
        this.user$ = userService.getUser();
        this.user$.subscribe( data => {  
            if ( data != null ) {                 
                this.idEmpresa = data.Empresa
            }                
        });   
    }

    

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let id = route.params['id']
        if(id){
            return this.chartService.getConjuntoDadosBd(id).toPromise()
        }else{
            return this.chartService.getConjuntoDadosBd(this.idEmpresa).toPromise()
        
        }
    }

}