
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { ActivatedRoute, Route } from '@angular/router';


@Component({
  selector: 'app-visualizar-dashboards',
  templateUrl: './visualizar-dashboards.component.html',
  styleUrls: ['./visualizar-dashboards.component.scss'],
})
export class VisualizarDashboardsComponent implements OnInit {
  
  minimizer: HTMLElement;

  public loading=true;
  public chartsData;

  constructor(
    private route: ActivatedRoute
  ){
    
  }

   
  ngOnInit(): void {

    this.minimizer = document.getElementById('sidebarMinimizer') as HTMLElement;
    // this.minimizer.click()
    setTimeout(() => {this.minimizer.click()}, 100);

    this.route.data.subscribe(data => { 
      if (typeof data.dados[0] == 'undefined'){
        this.chartsData = data.dados
      }else{
        this.chartsData = false
      }
      this.loading=false;
    })

  }

}

