
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Subject, observable, Observable } from 'rxjs';

import { ChartsService } from '../../../../services/charts.service';
import { ConjuntoObjtSerie } from '../../../../models/charts/conjuntoObjtSerie';
import { ObjtSerie } from '../../../../models/charts/objtSerie';
import { BdUpload } from '../../../../models/bd-upload';
import { MesesNumero } from '../../../../models/charts/mesesNumero';
import { User } from '../../../../models/user';
import { UserService } from '../../../../services/user.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-charts-balanco-patrimonial',
  templateUrl: './charts-balanco-patrimonial.component.html',
  // styleUrls: ['./balanco-patrimonial.component.scss'],
})
export class ChartsBalancoPatrimonialComponent implements OnInit {

  minimizer: HTMLElement;
  
  @Input('dados')
  public dados = null;
  
  public dtTrigger: Subject<any> = new Subject()
  public atributos = Object.getOwnPropertyNames( new BdUpload() ); 
  public user$: Observable<User>;
  public empresas;
  //public empresasSelect = { empresa_id:0, nome_fantasia: '' };

  /*  VALORES PARA OS CHARTS  */
  public valorChart_ReceitasOperacionais;  
  public valorChart_PesoCustoDespesa;  
  public valoresAuxiliares_ReceitasOperacionais;

  /* VARIAVEIS DENTRO DOS CHARTS */
  public receitasOperacionais_VariacaoMes;
  public receitasOperacionais_MediaAno;
  public receitasOperacionais_AcumuladoAno;

  /* MESES SELECT*/
  public mesesCadastrados = []
  public mesesCadastradosSelect = {id: 0, mes: ''}
  
  constructor(
    private userService: UserService,
    private chartsService : ChartsService,
    private _route: ActivatedRoute,

  ) { this.user$ = userService.getUser(); }

  onSelectReceitasOperacionais(event){

    this.receitasOperacionais_VariacaoMes = 50;
    this.valoresAuxiliares_ReceitasOperacionais.forEach(element => {
      new MesesNumero().meses.forEach( val => {
        if ( (val.mes.includes(event.slice(0,3) ) && (element.mes.slice(0,3) == event.slice(0,3)) ) ) {
          this.receitasOperacionais_VariacaoMes = element.valores.variacaoMes
          this.receitasOperacionais_AcumuladoAno = element.valores.acumuladoAno
          this.receitasOperacionais_MediaAno = element.valores.mediaAno
        }      
      })
      if ( element.mes )
      console.log( this.valoresAuxiliares_ReceitasOperacionais )
    });
    
  }

  
  mudarMesAnaliseGrafico(value, grafico){

    // this.valorChart_GastosTotais = this.todosValoresGastosTotais[value]
    // this.valorChart_LucroLiquidoMensal = this.todosValoresChartLucroLiquidoMensal[value]
    // this.valorChart_DeducaoDaReceita = this.todosValoresDeducaoDaReceita[value]

    // if ( grafico == 'LucroLiquidoMensal' ){
    //   this.valorChart_LucroLiquidoMensal = this.todosValoresChartLucroLiquidoMensal[value]
    // }
    // else if ( grafico == 'gastosTotais' ){
    //   this.valorChart_GastosTotais = this.todosValoresGastosTotais[value]
    // }
    // else if ( grafico == 'DeducaoDaReceita' ){
    //   this.valorChart_DeducaoDaReceita = this.todosValoresDeducaoDaReceita[value]
    // }
  }

  constroiArrayMeses(data){
    //Monta array de meses para o Ng-Select 
    let meses = [];
    data.forEach((coluna, i) => {
      meses.push({
        id: i,
        mes: coluna.Mes + '/' + coluna.Ano
      });
    });
    this.mesesCadastrados = meses;
  }

  //fica
  VerticalBar_ReceitasOperacionais (data){
    let conjuntoCompleto = [];
    let valoresAuxiliares = [];
    let mesCalculo = new MesesNumero();
    data.forEach((element, i) => { 
      conjuntoCompleto.push( new ObjtSerie( element.NomeColuna , (element.ReceitasOperacionais > 0) ? element.ReceitasOperacionais : element.ReceitasOperacionais * -1) );
      let valores = {
        "variacaoMes": (i > 0) ? Number((element.ReceitasOperacionais - data[i-1].ReceitasOperacionais).toFixed(2)) : Number(element.ReceitasOperacionais.toFixed(2)),
        "mediaAno": 0,
        "acumuladoAno": (i > 0) ? Number((element.ReceitasOperacionais + valoresAuxiliares[i-1].valores.acumuladoAno).toFixed(2)) : Number(element.ReceitasOperacionais.toFixed(2))
      }
      mesCalculo.meses.forEach( val => {        
        val.mes.includes(element.NomeColuna.slice(0, 3)) ? valores.mediaAno = Number((valores.acumuladoAno / val.valor).toFixed(2)) : false;
      })
      valoresAuxiliares.push({
        "mes": element.NomeColuna,
        "valores" : valores
      })
    });
    this.valorChart_ReceitasOperacionais = conjuntoCompleto; 
    this.valoresAuxiliares_ReceitasOperacionais = valoresAuxiliares;
  }
  //fica
  LineChart_PesoCustoDespesa(data){
    let conjuntoCompleto = []
    let conjuntoCusto = []
    let conjuntoDespesas = []    
    data.forEach(element => { 
      let somaCusto = (element.CustosOperacionais / element.ReceitasOperacionais);
      let somaDespesa = (element.DespesasOperacionais_somas / element.ReceitasOperacionais);

      conjuntoCusto.push( new ObjtSerie( element.NomeColuna , ( somaCusto > 0) ? somaCusto * 100 : somaCusto * -100) );
      conjuntoDespesas.push( new ObjtSerie( element.NomeColuna , ( somaDespesa > 0) ? somaDespesa * 100 : somaDespesa * -100) );
    });
    conjuntoCompleto.push( new ConjuntoObjtSerie('Peso Custo', conjuntoCusto) )      
    conjuntoCompleto.push( new ConjuntoObjtSerie('Peso Despesa', conjuntoDespesas) )      
    this.valorChart_PesoCustoDespesa = conjuntoCompleto;        
  }

  selectEmpresa(idEmpresa){

    this.chartsService.getConjuntoDadosBd(idEmpresa).subscribe( data => {       
      //  NgSelect meses
      this.mesesCadastrados = data['mensal']['meses'];

      //  Receitas Operacionais
      this.valorChart_ReceitasOperacionais = data['mensal']['ReceitasOperacionais']; 

      //  Peso Custo Despesa
      this.valorChart_PesoCustoDespesa = data['mensal']['PesoCustoDespesa']; 
    });
  }

  ngOnInit() {

    this.minimizer = document.getElementById('sidebarMinimizer') as HTMLElement;

    let id: number = Number(this._route.snapshot.paramMap.get('id'));
    if(id){
      this.selectEmpresa(id)
    }else{
      this.user$.subscribe( data => {  
        if ( data ) {
          if(data.Contabilidade != null){
            this.chartsService.getEmpresasPorContabilidade(data.Contabilidade).subscribe( data => {
              this.empresas = data;     
            })
          }else{  
            this.selectEmpresa(data.Empresa)
          }
        }
      });
    }

    setTimeout(() => {this.minimizer.click()}, 300);

   

  }





// options
view30: any[] = [0,0]// [(window.innerWidth * 0.40), (window.innerWidth * 0.30)]; 
view50: any[] = [0,0]// [(window.innerWidth * 0.5), 400]; 
showXAxis = true;
showYAxis = true;
gradient = false;
showLegend = true;
showXAxisLabel = true;
xAxisLabel = 'xAxisLabel';
showYAxisLabel = true;
yAxisLabel = 'yAxisLabel';
timeline = true;
label = "Label";
animations = true
showLabels = true;
explodeSlices = false;
doughnut = false;
colorScheme = {
  domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#6b00e9']
};
showDataLabel = true;


}

