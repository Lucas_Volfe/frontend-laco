import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhaContabilidadeComponent } from './minha-contabilidade.component';

describe('MinhaContabilidadeComponent', () => {
  let component: MinhaContabilidadeComponent;
  let fixture: ComponentFixture<MinhaContabilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhaContabilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhaContabilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
