import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancoPatrimonialComponent } from './balanco-patrimonial.component';

describe('BalancoPatrimonialComponent', () => {
  let component: BalancoPatrimonialComponent;
  let fixture: ComponentFixture<BalancoPatrimonialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalancoPatrimonialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancoPatrimonialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
