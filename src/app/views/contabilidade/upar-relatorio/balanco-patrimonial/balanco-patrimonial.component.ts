import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from '../../../../models/user';
import { UserService } from '../../../../services/user.service';
import { UploadDadosService } from '../../../../services/upload-dados.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BalancoPatrimonial } from '../../../../models/balanco-patrimonial';

@Component({
  selector: 'app-balanco-patrimonial',
  templateUrl: './balanco-patrimonial.component.html',
  styleUrls: ['./balanco-patrimonial.component.scss']
})
export class BalancoPatrimonialComponent implements OnInit {

  @Input('mesesRegistrados')
  public mesesRegistrados = null;

  @Input('dados')
  public dados = null;

  public registerForm: FormGroup;
  public empresa_id;
  public empresas;
  public user$: Observable<User>;
  public user;
  public mesSelect;

  constructor(
    private formBuilder: FormBuilder,
    private _uploadDadosService: UploadDadosService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router:Router,
    private toastr: ToastrService

  ) { 
    this.user$ = userService.getUser();
  }



  save(){
    let form = this.registerForm.getRawValue();
    form.Data_Upload = new Date()
    this._uploadDadosService.createBalancoPatrimonial(form).subscribe(response => {
      this.toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
      this.router.navigate(['']);

    }, err=>{
      this.toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
    })
  }

  selectMonthYear(event){
    this.registerForm.controls.Mes.setValue(event.getMonth()+1)
    this.registerForm.controls.Ano.setValue(event.getFullYear())
  }

  selectMesEdit(mesId, index){
    // console.log(index, this.dados)
    // console.log(mesId, this.mesesRegistrados)

    this.dados.forEach(data => {
      if (data.id == mesId){
        this.createForm(data);
      }
    });
    
    let date = this.mesesRegistrados[index].mes.split('/');
    this.registerForm.controls.Mes.setValue(date[0]);
    this.registerForm.controls.Ano.setValue(date[1]);
  }

  

  ngOnInit() {
    let id: number = Number(this.route.snapshot.paramMap.get('id'));
    this.createForm(null)    
  }

  createForm(values){   
    
    if(!values){
      values = new BalancoPatrimonial()
    }  

    this.registerForm = this.formBuilder.group({
      id: values.id,
      Mes: [values.Mes, Validators.required],
      Ano: [values.Ano, Validators.required],
      Data_Upload: values.Data_Upload,
      Empresa: this.route.snapshot.paramMap.get('id'),

      circulante_Ativo: values.circulante_Ativo ,
      disponivel: values.disponivel ,
      bens_Numerarios: values.bens_Numerarios ,
      depositos_Bancarios_A_Vista: values.depositos_Bancarios_A_Vista ,
      aplicacoes_De_Liquidez_Imediata: values.aplicacoes_De_Liquidez_Imediata ,
      clientes: values.clientes ,
      duplicatas_A_Receber: values.duplicatas_A_Receber ,
      duplicatas_Descontadas: values.duplicatas_Descontadas ,
      outros_Creditos: values.outros_Creditos ,
      titulos_A_Receber: values.titulos_A_Receber ,
      adiantamentos_A_Funcionarios: values.adiantamentos_A_Funcionarios ,
      tributos_A_Recuperar: values.tributos_A_Recuperar ,
      estoques: values.estoques ,
      estoque_Diversos: values.estoque_Diversos ,
      nao_Circulante_Ativo: values.nao_Circulante_Ativo ,
      realizavel_A_Longo_Prazo: values.realizavel_A_Longo_Prazo ,
      outros_Creditos_Longo_Prazo: values.outros_Creditos_Longo_Prazo ,
      creditos_De_Coligadas_E_Controladas: values.creditos_De_Coligadas_E_Controladas ,
      investimentos: values.investimentos ,
      outros_Investimentos: values.outros_Investimentos ,
      imobilizado: values.imobilizado ,
      imoveis: values.imoveis ,
      bens_Em_Operacao: values.bens_Em_Operacao ,
      depreciacao_Amortizacao_Exaustao_Acumulada: values.depreciacao_Amortizacao_Exaustao_Acumulada ,
      total_Do_Ativo: values.total_Do_Ativo ,
      circulante_Passivo: values.circulante_Passivo ,
      emprestimos: values.emprestimos ,
      financiamentos_Sistema_Financeiro_Nacional: values.financiamentos_Sistema_Financeiro_Nacional ,
      fornecedores: values.fornecedores ,
      fornecedores_Nacionais: values.fornecedores_Nacionais ,
      obrigacoes_Tributarias_Circulante: values.obrigacoes_Tributarias_Circulante ,
      impostos_E_Contribuicoes_A_Recolher: values.impostos_E_Contribuicoes_A_Recolher ,
      tributos_Retidos_A_Recolher: values.tributos_Retidos_A_Recolher ,
      obrigacoes_Trabalhistas_E_Prividenciarias: values.obrigacoes_Trabalhistas_E_Prividenciarias ,
      obrigacoes_Com_O_Pessoal: values.obrigacoes_Com_O_Pessoal ,
      obrigacoes_Previdenciarias: values.obrigacoes_Previdenciarias ,
      provisoes: values.provisoes ,
      outras_Obrigacoes: values.outras_Obrigacoes ,
      contas_A_Pagar: values.contas_A_Pagar ,
      contas_Correntes: values.contas_Correntes ,
      dividendos_Participacoes_Juros_Capital_Proprio: values.dividendos_Participacoes_Juros_Capital_Proprio ,
      juros_Sobre_O_Capital_Proprio: values.juros_Sobre_O_Capital_Proprio ,
      nao_Circulante_Passivo: values.nao_Circulante_Passivo ,
      obrigacoes_A_Longo_Prazo: values.obrigacoes_A_Longo_Prazo ,
      instituicoes_Financeiras: values.instituicoes_Financeiras ,
      financiamentos: values.financiamentos ,
      obrigacoes_Tributarias_Nao_Circulante: values.obrigacoes_Tributarias_Nao_Circulante ,
      impostos_E_Contribuicoes: values.impostos_E_Contribuicoes ,
      patrimonio_Liquido: values.patrimonio_Liquido ,
      capital_Social: values.capital_Social ,
      capital_Subscrito: values.capital_Subscrito ,
      lucros_E_Projuizos_Acumulados_Soma: values.lucros_E_Projuizos_Acumulados_Soma ,
      lucros_E_Projuizos_Acumulados: values.lucros_E_Projuizos_Acumulados ,
      lucros_E_Prejuizos_Exercicio: values.lucros_E_Prejuizos_Exercicio ,
      total_Do_Patrimonio_Liquido_E_Passivo: values.total_Do_Patrimonio_Liquido_E_Passivo ,
      instituicoes_Financeiras_Nao_circulante: values.instituicoes_Financeiras_Nao_circulante
    });  

  }


  somaCirculanteAtivo(){
    this.registerForm.controls.circulante_Ativo.setValue(
      this.registerForm.controls.disponivel.value +
      this.registerForm.controls.clientes.value +
      this.registerForm.controls.outros_Creditos.value +
      this.registerForm.controls.estoques.value
    ) 
  }
  changeDisponivel(){
    this.registerForm.controls.disponivel.setValue(
      this.registerForm.controls.bens_Numerarios.value +
      this.registerForm.controls.depositos_Bancarios_A_Vista.value +
      this.registerForm.controls.aplicacoes_De_Liquidez_Imediata.value
    ) 
    this.somaCirculanteAtivo();
  }

  changeCliente(){
    this.registerForm.controls.clientes.setValue(
      this.registerForm.controls.duplicatas_A_Receber.value +
      this.registerForm.controls.duplicatas_Descontadas.value
    ) 
    this.somaCirculanteAtivo();
  }

  changeOutrosCreditos(){
    this.registerForm.controls.outros_Creditos.setValue(
      this.registerForm.controls.titulos_A_Receber.value +
      this.registerForm.controls.adiantamentos_A_Funcionarios.value +
      this.registerForm.controls.tributos_A_Recuperar.value
    ) 
    this.somaCirculanteAtivo();

  }

  changeEstoque(){
    this.registerForm.controls.estoques.setValue(
      this.registerForm.controls.estoque_Diversos.value
    )
    this.somaCirculanteAtivo();

  }

  
  somaNaoCirculanteAtivo(){
    this.registerForm.controls.nao_Circulante_Ativo.setValue(
      this.registerForm.controls.realizavel_A_Longo_Prazo.value +
      this.registerForm.controls.investimentos.value +
      this.registerForm.controls.imobilizado.value 
    ) 
  }
     
  

  changeRealizavelALongoPrazo(){
    this.registerForm.controls.realizavel_A_Longo_Prazo.setValue(
      this.registerForm.controls.creditos_De_Coligadas_E_Controladas.value
    );
    this.registerForm.controls.outros_Creditos_Longo_Prazo.setValue(
      this.registerForm.controls.creditos_De_Coligadas_E_Controladas.value
    );   
    this.somaNaoCirculanteAtivo();
  }

  changeInvestimentos(){
    this.registerForm.controls.investimentos.setValue(
      this.registerForm.controls.outros_Investimentos.value
    );   
    this.somaNaoCirculanteAtivo();
  }

  changeImobilizado(){
    this.registerForm.controls.imobilizado.setValue(
      this.registerForm.controls.imoveis.value +
      this.registerForm.controls.bens_Em_Operacao.value +
      this.registerForm.controls.depreciacao_Amortizacao_Exaustao_Acumulada.value 
    );   
    this.somaNaoCirculanteAtivo();
  }

  somaCirculantePassivo(){
    this.registerForm.controls.circulante_Passivo.setValue(
      this.registerForm.controls.instituicoes_Financeiras.value +
      this.registerForm.controls.fornecedores.value +
      this.registerForm.controls.obrigacoes_Tributarias_Circulante.value +
      this.registerForm.controls.obrigacoes_Trabalhistas_E_Prividenciarias.value +
      this.registerForm.controls.outras_Obrigacoes.value +
      this.registerForm.controls.dividendos_Participacoes_Juros_Capital_Proprio.value 
    );   
  }

  changeInstituicoesFinanceiras(){
    this.registerForm.controls.instituicoes_Financeiras.setValue(
      this.registerForm.controls.emprestimos.value +
      this.registerForm.controls.financiamentos_Sistema_Financeiro_Nacional.value 
    );   
    this.somaCirculantePassivo();
  }

  changeFornecedores(){
    this.registerForm.controls.fornecedores.setValue(
      this.registerForm.controls.fornecedores_Nacionais.value 
    );   
    this.somaCirculantePassivo();
  }

  changeObrigacoesTributarias(){
    this.registerForm.controls.obrigacoes_Tributarias_Circulante.setValue(
      this.registerForm.controls.impostos_E_Contribuicoes_A_Recolher.value +
      this.registerForm.controls.tributos_Retidos_A_Recolher.value 
    );   
    this.somaCirculantePassivo();
  }
  
  changeObrigacoesTrabalhistas(){
    this.registerForm.controls.obrigacoes_Trabalhistas_E_Prividenciarias.setValue(
      this.registerForm.controls.obrigacoes_Com_O_Pessoal.value +
      this.registerForm.controls.provisoes.value +
      this.registerForm.controls.obrigacoes_Previdenciarias.value 
    );   
    this.somaCirculantePassivo();
  }
  
  changeOutrasObrigacoes(){
    this.registerForm.controls.outras_Obrigacoes.setValue(
      this.registerForm.controls.contas_A_Pagar.value +
      this.registerForm.controls.contas_Correntes.value 
    );   
    this.somaCirculantePassivo();
  }
  
  changeDividendos(){
    this.registerForm.controls.dividendos_Participacoes_Juros_Capital_Proprio.setValue(
      this.registerForm.controls.juros_Sobre_O_Capital_Proprio.value 
    );   
    this.somaCirculantePassivo();
  }
  
  somaNaoCirculantePassivo(){
    this.registerForm.controls.nao_Circulante_Passivo.setValue(
      this.registerForm.controls.obrigacoes_A_Longo_Prazo.value +
      this.registerForm.controls.patrimonio_Liquido.value 
    );   
  }

  somaObrigacoesLongoPrazo(){
    
    this.registerForm.controls.instituicoes_Financeiras_Nao_circulante.setValue(
      this.registerForm.controls.financiamentos.value 
    );
      
    this.registerForm.controls.obrigacoes_Tributarias_Nao_Circulante.setValue(
      this.registerForm.controls.impostos_E_Contribuicoes.value 
    );

    this.registerForm.controls.obrigacoes_A_Longo_Prazo.setValue(
      this.registerForm.controls.instituicoes_Financeiras_Nao_circulante.value +
      this.registerForm.controls.obrigacoes_Tributarias_Nao_Circulante.value 
    );
    this.somaNaoCirculantePassivo();   
  }

  somaPatrimonioLiquido(){
    this.registerForm.controls.patrimonio_Liquido.setValue(
      this.registerForm.controls.capital_Social.value +
      this.registerForm.controls.lucros_E_Projuizos_Acumulados_Soma.value 
    );  
  }

  changeCapitalSocial(){
    this.registerForm.controls.capital_Social.setValue(
      this.registerForm.controls.capital_Subscrito.value 
    );  
    this.somaPatrimonioLiquido()
  }
  changeLucroPrejuizoAcumulado(){
    this.registerForm.controls.lucros_E_Projuizos_Acumulados_Soma.setValue(
      this.registerForm.controls.lucros_E_Projuizos_Acumulados.value  +
      this.registerForm.controls.lucros_E_Prejuizos_Exercicio.value  
    );  
    this.somaPatrimonioLiquido()
  }
}
