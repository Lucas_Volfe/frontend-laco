import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UparRelatorioComponent } from './upar-relatorio.component';

describe('UparRelatorioComponent', () => {
  let component: UparRelatorioComponent;
  let fixture: ComponentFixture<UparRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UparRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UparRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
