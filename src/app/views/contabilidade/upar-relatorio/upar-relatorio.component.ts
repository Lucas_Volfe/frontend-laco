import { Component, OnInit, ViewContainerRef  } from '@angular/core';
import { ToastrService } from 'ngx-toastr';    
import { Subject, Observable } from 'rxjs';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  TreeviewI18n, TreeviewItem, TreeviewConfig, TreeviewHelper, TreeviewComponent,
  TreeviewEventParser, OrderDownlineTreeviewEventParser, DownlineTreeviewItem
} from 'ngx-treeview';
import { isNil, remove, reverse } from 'lodash';
import { Router } from '@angular/router';

//import {  } from '@qontu/ngx-inline-editor';

import { BdUpload } from '../../../models/bd-upload';
import { UploadDadosService } from '../../../services/upload-dados.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { colorSets } from '@swimlane/ngx-charts/release/utils';



@Component({
  selector: 'app-upar-relatorio',
  templateUrl: './upar-relatorio.component.html',
  styleUrls: ['./upar-relatorio.component.scss'],
  providers: [UploadDadosService, NgbModalConfig, NgbModal,
    { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser },
    { provide: TreeviewConfig }
  ]

})
export class UparRelatorioComponent implements OnInit {
  public items: TreeviewItem[];
  public rows: string[];
  public config = TreeviewConfig.create({
      hasAllCheckBox: true,
      hasFilter: false,
      hasCollapseExpand: false,
      decoupleChildFromParent: false,
      maxHeight: 400
  });
  public habilitar_btn_enviar = 'true';
  public dtTrigger: Subject<any> = new Subject();
  public arquivo = [];
  public objetoBd = new BdUpload(); 
  public colunasCsv: BdUpload[] = [];
  public colunasCsv_paraUpload: BdUpload[] = [];
  public uploadsRegistrados;  
  public user$: Observable<User>;
  public empresas;
  public empresaSelect = { id:0, Nome_Fantasia: '' };
  public empresa_id;
  public user;
  
  
  constructor(
    private _toastr: ToastrService,
    private _uploadDadosService: UploadDadosService,
    config: NgbModalConfig, 
    private modalService: NgbModal,
    private userService: UserService,
    private _router: Router,
    
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    this.user$ = userService.getUser();
  }

  onSelectedChange(downlineItems: DownlineTreeviewItem[]) {
    this.rows = [];
    this.colunasCsv_paraUpload = [];
    
    downlineItems.forEach(downlineItem => {
        const item = downlineItem.item;
        const value = item.value;
        const texts = [item.text];
        let parent = downlineItem.parent;
        while (!isNil(parent)) {
            texts.push(parent.item.text);
            parent = parent.parent;
        }
        const reverseTexts = reverse(texts);
        const row = `${reverseTexts.join(' -> ')} : ${value}`;
        this.rows.push(row);

        this.colunasCsv_paraUpload.push(this.colunasCsv[item.value]);
    });
  }

  mudarValor(novoValor, itemEditado){
    console.log(novoValor, itemEditado)
  }

  formatarValorExcelToChart(valor: string){
    valor = valor.replace(/([.])/gi, '' );
    valor = valor.replace(/([,])/gi, '.'); 
    //valor = valor.replace(/([-])/gi, '0'); 
    
    return valor;
  }

  onFilesAdded(files: File[]) {
    files.forEach(file => {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        const content = (e.target as FileReader).result; 
        const format = content.toString().split( String.fromCharCode(10) ) ;        
        this.separarColunasUpload( format ) 
        this.habilitar_btn_enviar = null;

      };
      reader.readAsText(file);
    });

  }

  separarColunasUpload(data){
    let atributos = Object.getOwnPropertyNames(this.objetoBd); 
    let colunas: BdUpload[] = []
    let i = 1;
    
    data.forEach(row => {  
      let linha = row.replace(/([' '])/gi, '').split(';');  
      let k = 0;
      //console.log(linha);
      //Criar Objeto para cada Coluna
      linha.forEach(celula => {  
        if ( i == 1 ){
          let col = new BdUpload();
          col.Mes = celula.split('-')[0];
          col.Ano = celula.split('-')[1];
          col.Data_Upload = new Date();
          col.Empresa = this.empresa_id;
          colunas.push(col);
        }else{               
          colunas[k][atributos[i+2]] = Number(this.formatarValorExcelToChart(celula));           
        }
        k++;  
      });
      i++;
    });
    
    let JsonMyAPI = [];
    i = 0;
    colunas.forEach( coluna => {
      colunas[i] = coluna.somasCelulasFormulasExcel(coluna);
      JsonMyAPI.push( JSON.stringify(colunas[i]) );
      //console.log(JSON.stringify(colunas[i]) + ',')   
      i++
    });

    //console.log(colunas)

    this.colunasCsv = colunas.slice(2);
    
    document.querySelector( '#tableUpload' ).classList.remove('d-none');
    document.querySelector( '#tableUpload' ).classList.add('d-block');
  }

  checkUploadsRealizados(data){
    data.forEach(coluna => {
      this.uploadsRegistrados.forEach(check => {
        if ( coluna.Ano == check.Ano && coluna.Mes == check.Mes ){
          coluna['id'] = check.id; 
          coluna['Empresa'] = this.empresa_id;
        }
      });
    });
  }

  constroiTreeView(colunas){    
    let children_existentes = [];
    let children_novos = [];
    Object.entries(colunas).forEach(      
      ([key, coluna]) => {
        let new_children = {
          text: coluna['Mes'], value: key
        };
        ( coluna['id'] ) ?
          children_existentes.push(new_children) :
          children_novos.push(new_children) ; 
      }
    );
    let existentes = new TreeviewItem({
      text: 'Dados Existentes', value: 1, children: children_existentes
    });
    let novos = new TreeviewItem({
      text: 'Novos Valores', value: 2, children: children_novos
    });

    return [ existentes, novos ]
  } 

  onFilesRejected(files: File[]) {
    console.log(files);
  }
 
  processoEnvio(content) {
    if ( this.uploadsRegistrados ){
     console.log(content)

      this.modalService.open(content);
      this.checkUploadsRealizados(this.colunasCsv) 
      this.items = this.constroiTreeView(this.colunasCsv);   
    } 
    else {
      this.colunasCsv_paraUpload = this.colunasCsv;
      this.colunasCsv_paraUpload.forEach(element => {
        element.Empresa = this.empresa_id;
      });
      this.submit();
    }
  }
  
  submit() {  
    this._uploadDadosService.create(this.colunasCsv_paraUpload).subscribe(data => {
      //this.itens.push(data);
      this._toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
      setTimeout(() => {
        this.modalService.dismissAll()
        this._router.navigate(['contabilidade']);        
      }, 1300);
    }, error => {
      console.log(error);
      this._toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
    });    
  }

  checkTodosUploads(empresa_id){
    this.empresa_id = empresa_id;
    this._uploadDadosService.getDisponibilidadeUpload(empresa_id).subscribe( data => {
      this.uploadsRegistrados=[];
      this.uploadsRegistrados = data;
    })
  }

  checkTodasEmpresasDaContabilidade(contabilidade_id){
    this._uploadDadosService.getEmpresasPorContabilidade(contabilidade_id).subscribe( data => {
      this.empresas = data;
    })
  }

  

  ngOnInit() {
    this.user$.subscribe( data => {   
      if( data ) {
        this.user = data;
        this.checkTodasEmpresasDaContabilidade(data.Contabilidade);
      }
    });
   

  
  }

 
  
}
