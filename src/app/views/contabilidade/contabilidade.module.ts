import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { DataTablesModule } from 'angular-datatables';
import { NgxChartsModule,
  BaseChartComponent,
  LineComponent,
  LineSeriesComponent,
  calculateViewDimensions,
  ViewDimensions,
  ColorHelper,
  formatLabel } from '@swimlane/ngx-charts';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgSelectModule } from '@ng-select/ng-select';

import { NgxClickToEditModule } from 'ngx-click-to-edit';
import { InlineEditorModule } from '@qontu/ngx-inline-editor';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TreeviewModule } from 'ngx-treeview';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import { ContabilidadeRoutingModule } from './contabilidade-routing.module';
import { VisualizarDashboardsComponent } from './visualizar-dashboards/visualizar-dashboards.component';
import { GerenciarDashboardsComponent } from './gerenciar-dashboards/gerenciar-dashboards.component';
import { IndexComponent } from './index/index.component';
import { GerenciarClientesComponent } from './gerenciar-clientes/gerenciar-clientes.component';
import { GerenciarUploadsComponent } from './gerenciar-uploads/gerenciar-uploads.component';
import { UparRelatorioComponent } from './upar-relatorio/upar-relatorio.component';
import { TreinamentoComponent } from './treinamento/treinamento.component';
import { HistoricoUploadsComponent } from './historico-uploads/historico-uploads.component';
import { MinhaContabilidadeComponent } from './minha-contabilidade/minha-contabilidade.component';
import { CadastroModule } from '../cadastro/cadastro.module';
import { BalancoPatrimonialComponent } from './upar-relatorio/balanco-patrimonial/balanco-patrimonial.component';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMaskModule } from 'ngx-mask';
import { ChartsBalancoPatrimonialComponent } from './visualizar-dashboards/charts-balanco-patrimonial/charts-balanco-patrimonial.component';
import { ChartsDREComponent } from './visualizar-dashboards/charts-dre/charts-dre.component';
import { VisualizarDashboardsResolver } from './visualizar-dashboards/visualizar-dashboards.resolver';
import { TableBPComponent } from './gerenciar-uploads/table-bp/table-bp';
import { TableDREComponent } from './gerenciar-uploads/table-dre/table-dre.component';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatGridListModule} from '@angular/material/grid-list';
import { ComboChartComponent, ComboSeriesVerticalComponent } from './visualizar-dashboards/combo-chart';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  imports: [
    CommonModule,
    ContabilidadeRoutingModule,
    ChartsModule,
    DataTablesModule,
    NgxDropzoneModule,
    InlineEditorModule,
    NgxClickToEditModule.forRoot(),
    NgxChartsModule,
    TabsModule,
    NgSelectModule,
    NgbModule,
    TreeviewModule.forRoot(),     
    FormsModule,
    CadastroModule,
    NgxLoadingModule.forRoot({}),
    MatListModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaskModule.forRoot(),
    MatTabsModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatGridListModule,
    MatIconModule

  ],
  declarations: [ 
    VisualizarDashboardsComponent, 
    GerenciarDashboardsComponent, 
    IndexComponent, 
    GerenciarClientesComponent, 
    GerenciarUploadsComponent, 
    UparRelatorioComponent, 
    TreinamentoComponent, 
    HistoricoUploadsComponent, 
    MinhaContabilidadeComponent, 
    BalancoPatrimonialComponent, 
    ChartsBalancoPatrimonialComponent,
    ChartsDREComponent,
    TableBPComponent,
    TableDREComponent,
    ComboChartComponent,
    ComboSeriesVerticalComponent
  ],
  exports: [
    HistoricoUploadsComponent,
    VisualizarDashboardsComponent,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }, 
    VisualizarDashboardsResolver
  ]
})
export class ContabilidadeModule { }
