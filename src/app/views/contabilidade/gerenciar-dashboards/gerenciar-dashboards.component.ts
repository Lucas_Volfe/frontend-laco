import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';

import { GerenciarDashboardsService } from './../../../services/gerenciar-dashboards.service';
import { Table } from '../../../models/table';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';



@Component({
  selector: 'app-gerenciar-dashboards',
  templateUrl: './gerenciar-dashboards.component.html',
  styleUrls: ['./gerenciar-dashboards.component.scss']
})

export class GerenciarDashboardsComponent implements OnInit, OnDestroy {

  @Input() variavelGlobal;
  
  public user$: Observable<User>;
  public uploads: Table[];
  public dtTrigger: Subject<any> = new Subject();
  public tableTemDados = false;
  public config = { backdropBackgroundColour: 'rgba(0, 0, 0, 0)' }
  public loading = true;

  constructor(
    private router: Router,
    private dashboardsService : GerenciarDashboardsService,
    private userService: UserService,

    ) { this.user$ = userService.getUser(); }

  dtOptions: DataTables.Settings={};

  selecionaEmpresa(empresaId){
    console.log(empresaId)
  }

  ngOnInit(): void {     

    this.dtOptions.language = {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
    };

    this.user$.subscribe( data => {
      if (data && (data.Contabilidade != null)) {
        this.dashboardsService.getEmpresasPorContabilidade((data.Contabilidade)).subscribe( data => {
          //console.log(data)
          this.loading = false;
          if ( data[0] != null ) {
            this.tableTemDados = true;
            this.uploads = data;
            this.dtTrigger.next();
            //console.log(data)
          }
        }, error => {
          console.log(error);
        });
      } 
    });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  

}
