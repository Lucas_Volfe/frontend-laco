import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarDashboardsComponent } from './gerenciar-dashboards.component';

describe('GerenciarDashboardsComponent', () => {
  let component: GerenciarDashboardsComponent;
  let fixture: ComponentFixture<GerenciarDashboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarDashboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarDashboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
