import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarTreinamentosComponent } from './gerenciar-treinamentos.component';

describe('GerenciarTreinamentosComponent', () => {
  let component: GerenciarTreinamentosComponent;
  let fixture: ComponentFixture<GerenciarTreinamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarTreinamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarTreinamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
