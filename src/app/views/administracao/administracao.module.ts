import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { GerenciarContabilidadeComponent } from './gerenciar-contabilidade/gerenciar-contabilidade.component';
import { GerenciarTreinamentosComponent } from './gerenciar-treinamentos/gerenciar-treinamentos.component';
import { AdministracaoRoutingModule } from './administracao-routing.module';
import { CadastroModule } from '../cadastro/cadastro.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { ContabilidadeModule } from '../contabilidade/contabilidade.module';
import { MatSliderModule } from '@angular/material/slider';
import { ChartsModule } from 'ng2-charts';
import { IndexResolver } from './index/index.resolver';
import { NgxMaskModule } from 'ngx-mask';
import { NgxLoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    IndexComponent, 
    GerenciarContabilidadeComponent, 
    GerenciarTreinamentosComponent, 
  ],
  imports: [
    CommonModule,
    AdministracaoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CadastroModule,
    NgbModule,
    DataTablesModule,
    ContabilidadeModule,
    MatSliderModule,
    ChartsModule,
    NgxMaskModule.forRoot(),
    NgxLoadingModule

  ],
  providers:[
    IndexResolver
  ]

})
export class AdministracaoModule { }
