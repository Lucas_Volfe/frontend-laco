import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarContabilidadeComponent } from './gerenciar-contabilidade.component';

describe('GerenciarContabilidadeComponent', () => {
  let component: GerenciarContabilidadeComponent;
  let fixture: ComponentFixture<GerenciarContabilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarContabilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarContabilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
