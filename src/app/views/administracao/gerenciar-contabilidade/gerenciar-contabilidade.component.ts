import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { GerenciarContabilidadeService } from '../../../services/gerenciar-contabilidade.service';
import { Contabilidade } from '../../../models/contabilidade';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gerenciar-contabilidade',
  templateUrl: './gerenciar-contabilidade.component.html',
  styleUrls: ['./gerenciar-contabilidade.component.scss']
})
export class GerenciarContabilidadeComponent implements OnInit {

 
  public dtTrigger: Subject<any> = new Subject();
  public contabilidades: Contabilidade[];
  public user$: Observable<User>;
  public dtOptions: DataTables.Settings = {};
  public loading = true;


  constructor( 
    private userService: UserService,
    private contabilidadeService : GerenciarContabilidadeService,
    private router: Router
   ) { this.user$ = userService.getUser() }
  
  goToAccounting(id){
    this.userService.getIdentityAndSetExpires(id);    
    this.router.navigate(['']);
  }

  ngOnInit() {    
    this.contabilidadeService.getContabilidades().subscribe( data => {          
      this.contabilidades = data;
      this.loading = false;
      this.dtTrigger.next();          
    }, error => {
      console.log(error);
    });
    this.dtOptions.language = {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
    }

  }




}
