import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ChartsService } from '../../../services/charts.service';
import { UserService } from '../../../services/user.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../../../models/user';
import { DashboardsAdminService } from '../../../services/dashboards-admin.service';

@Injectable()
export class IndexResolver implements Resolve<any>{
    

    public onChange: BehaviorSubject<any>

    constructor(
        private dashboardsAdminService:DashboardsAdminService,
    )
    {         
        this.onChange = new BehaviorSubject([]);
    }

    

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.onChange.next(await this.dashboardsAdminService.getResults() )
    }

}