import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { GerenciarContabilidadeComponent } from './gerenciar-contabilidade/gerenciar-contabilidade.component';
import { GerenciarTreinamentosComponent } from './gerenciar-treinamentos/gerenciar-treinamentos.component';
import { FormularioNovoUsuarioComponent } from '../cadastro/formulario-novo-usuario/formulario-novo-usuario.component';
import { NovaContabilidadeComponent } from '../cadastro/nova-contabilidade/nova-contabilidade.component';
import { NovoTreinamentoComponent } from '../cadastro/novo-treinamento/novo-treinamento.component';
import { VisualizarDashboardsComponent } from '../contabilidade/visualizar-dashboards/visualizar-dashboards.component';
import { IndexResolver } from './index/index.resolver';
import { TreinamentoComponent } from '../contabilidade/treinamento/treinamento.component';



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Administração'
    },
    children:[
      {
        path: '',
        component: IndexComponent,
        data: {
          title: 'Administração'
        },
        // resolve: { data: IndexResolver }
      },
      {
        path: 'nova-contabilidade',
        component: NovaContabilidadeComponent,
        data:{
          title: 'Nova Contabilidade'
        }
      },
      {
        path: 'nova-contabilidade/:id',
        component: NovaContabilidadeComponent,
        data:{
          title: 'Nova Contabilidade'
        }
      },
      {
        path: 'gerenciar-contabilidade',
        component: GerenciarContabilidadeComponent,
        data:{
          title: 'Gerenciar Contabilidade'
        }
      },
      {
        path: 'novo-treinamento',
        component: NovoTreinamentoComponent,
        data: {
          title: 'Novo Treinamento'
        }
      },
      {
        path: 'gerenciar-treinamento',
        component: TreinamentoComponent,
        data: {
          title: 'Gerenciar Treinamento'
        }
      },
      {
        path: 'dashboards/:id',
        component: VisualizarDashboardsComponent,
        data: {
          title: 'Visualizar Dashboards'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministracaoRoutingModule {}
