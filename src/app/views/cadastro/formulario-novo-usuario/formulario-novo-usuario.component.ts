import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';   
import { Router } from '@angular/router';
import { debounceTime, switchMap, map, first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RegisterService } from '../../../services/register.service';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { Empreendimento } from '../../../models/empreendimento';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-formulario-novo-usuario',
  templateUrl: './formulario-novo-usuario.component.html',
  styleUrls: ['./formulario-novo-usuario.component.scss'],
  providers: [UserNotTakenValidatorService]
})
export class FormularioNovoUsuarioComponent implements OnInit {
  
  @Input() fullComponent;
  @Input() idNovaContabilidade;
  @Output() validatorsOk = new EventEmitter();
  boolValue = true;
  
  
  
  public registerForm: FormGroup;
  public empresas:Empreendimento[];  
  public contabilidades:Empreendimento[];  
  public activeSelectEmpresa = false;
  public activeSelectContabilidade = false;
  public user$: Observable<User>;
  public user: User;
  

  constructor(
    private formBuilder: FormBuilder,
    private UserNotTakenValidatorService: UserNotTakenValidatorService,
    private registerService: RegisterService,
    private router:Router,
    private _toastr: ToastrService,
    private userService: UserService,
    
    ) { this.user$ = userService.getUser(); }    

    registrar(){
      const newUser = this.registerForm.getRawValue();      
      newUser.username = newUser.email;      
      if (newUser.Role == 1) {
        newUser.Contabilidade = null;
        newUser.Empresa = null;
      }  
      else if( newUser.Role == 2 && this.user.Contabilidade){
        newUser.Contabilidade = Number(this.user.Contabilidade);
        newUser.Empresa = null;
      }     

      this.registerService.createUser(newUser).subscribe( 
        () => {
          this.router.navigate(['']);
          this._toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
        }, 
        err => {
          this._toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
          console.log(err)
      });   
    }

    checkInputsPasswords(){
      return (control: AbstractControl) => {
        return control.valueChanges
          .pipe(debounceTime(300))
          .pipe(switchMap( rptPass => {    
            return (rptPass == this.registerForm.controls.password.value) ? ['true'] :['false']
          } ))
          .pipe(map( isTaken => {            
            return (isTaken == 'false') ? {passwordChecked: true} : null
          }))
          .pipe(first());
      }
    }     
    
    openSelectByRole(rolePerfil) {
      (rolePerfil == 3) ? 
        this.activeSelectEmpresa = true :
        this.activeSelectEmpresa = false;

      (rolePerfil == 2 && !this.user.Contabilidade) ? 
        this.activeSelectContabilidade = true :
        this.activeSelectContabilidade = false;
    }

    selectRole(){
      var form = this.registerForm.getRawValue()      
      this.registerForm.patchValue({
        Role: (!form.Contabilidade)? 1: 2
      })
    }

    ngOnInit(): void {

      if ( this.fullComponent == undefined) this.fullComponent = 'true';
      
      this.boolValue = (this.fullComponent == "true");
      
      this.user$.subscribe( data => {
        if(data){
          this.user = data;
          if(data.Contabilidade){
            this.registerService.getEmpresasRefetenteAContabilidade(data.Contabilidade).subscribe( data => {
              this.empresas = Object.keys(data).map(i => data[i]);
            })
          }else{
            this.registerService.getTodasContabilidades().subscribe(data => {
              this.contabilidades = Object.keys(data).map(i => data[i]);
            })
          }
        }
      });


      this.registerForm = this.formBuilder.group({
        first_name: ['', 
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
        ],
        /*username: ['', 
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
            Validators.pattern(/^[a-z0-9_\-]+$/)
          ],
          this.UserNotTakenValidatorService.checkUserNameTaken()
        ],*/
        email: ['', 
          [
            Validators.required,
            Validators.email
          ],
          this.UserNotTakenValidatorService.checkEmailTaken()
        ],
        password: ['', 
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(14)
          ],
          this.checkInputsPasswords()
        ],
        rptPassword: ['', 
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(14)
          ],
          this.checkInputsPasswords()
        ],
        Role: null,
        Empresa: null,
        Contabilidade: null
      });  

      this.registerForm.statusChanges.subscribe(value => {
        (value == 'VALID') ? this.validatorsOk.emit(this.registerForm.getRawValue()) : false;
      })   
      
    }
}
