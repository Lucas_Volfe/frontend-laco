import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { debounceTime, switchMap, map, first } from 'rxjs/operators';
import { RegisterService } from '../../../services/register.service';

@Injectable()
export class UserNotTakenValidatorService {

    constructor(private registerService: RegisterService) {}

    checkUserNameTaken() {
        return (control: AbstractControl) => {
            return control.valueChanges
                .pipe(debounceTime(300))
                .pipe(switchMap( user => this.registerService.checkUserNameTaken(user)))
                .pipe(map(isTaken => {
                    return Object.keys(isTaken).length > 0 ? {userNameTaken: true} : null
                }))
                .pipe(first());
        }
    }

    checkEmailTaken() {
        return (control: AbstractControl) => {
            return control.valueChanges
                .pipe(debounceTime(300))
                .pipe(switchMap( email => this.registerService.checkEmailTaken(email)))
                .pipe(map(isTaken => {
                    return Object.keys(isTaken).length > 0 ? {emailTaken: true} : null
                }))
                .pipe(first());
        }
    }
    
}