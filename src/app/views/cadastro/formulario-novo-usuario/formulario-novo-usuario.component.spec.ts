import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioNovoUsuarioComponent } from './formulario-novo-usuario.component';

describe('FormularioNovoUsuarioComponent', () => {
  let component: FormularioNovoUsuarioComponent;
  let fixture: ComponentFixture<FormularioNovoUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioNovoUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioNovoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
