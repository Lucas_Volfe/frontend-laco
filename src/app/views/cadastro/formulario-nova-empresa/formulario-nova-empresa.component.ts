import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';   
import { User } from '../../../models/user';
import { RegisterService } from '../../../services/register.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-formulario-nova-empresa',
  templateUrl: './formulario-nova-empresa.component.html',
  styleUrls: ['./formulario-nova-empresa.component.scss']
})
export class FormularioNovaEmpresaComponent implements OnInit {

  public file = null;
  public novoUsuario;
  public validatorsOkNovoUsuario = true;
  public registerForm: FormGroup;
  public user$: Observable<User>;
  public empresa_id:number;
  public status: number = 1;

  public statusList = [
    { id: 1, name: 'Ativo', checked: true },
    { id: 2, name: 'Teste', checked: false },
    { id: 3, name: 'Desabilitado', checked: false }
  ];
  
  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private router:Router,
    private toastr: ToastrService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute  

  ) { this.user$ = userService.getUser(); }
  
  fileToBase64(base){
    this.file = base;
  }

  onFilesAdded(files: File[]) {
    files.forEach(file => {
      const reader = new FileReader();
      reader.readAsDataURL(file);       
      reader.onload = (e: any)=>{
        this.fileToBase64(e.target.result)  
      }
    });     

  }

  registrar(){
    const novaEmpresa = this.registerForm.getRawValue();
    this.user$.subscribe( data => { novaEmpresa.Contabilidade = data.Contabilidade} );
    this.empresa_id ? novaEmpresa.id = this.empresa_id : false;
    let cnpjNovaEmpresa = novaEmpresa.Cnpj;
    novaEmpresa.status = this.statusList[this.status].name;
    if(this.activatedRoute.snapshot.paramMap.get('id')){
      this.registerService.createEmpresa(novaEmpresa, this.file).subscribe(()=>{
        this.toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
        this.router.navigate(['']);
      }, 
        err => {
          this.toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
          console.log(err)
      }) 

    }
    else{
      this.registerService.createEmpresa(novaEmpresa, this.file).subscribe( 
        () => {
          this.registerService.getEmpresas().subscribe(data => {
            data.forEach(item => {
              if(item.Cnpj == cnpjNovaEmpresa){
                this.novoUsuario.Empresa = item.id;
                this.registerService.createUser(this.novoUsuario).subscribe(()=>{
                  this.toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
                  this.router.navigate(['']);
                })
              } 
            })  
          })
        }, 
        err => {
          this.toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
          console.log(err)
      });

    }
  }

  reciverFeedback(novoUsuario) {
    novoUsuario.Role = 3;
    novoUsuario.username = novoUsuario.email;
    this.novoUsuario = novoUsuario
    this.validatorsOkNovoUsuario = false
  }

  selectStatus(idStatus){      
    this.statusList.forEach(status => {
      if(status.id == idStatus){
        this.status = idStatus;
        status.checked = true
      }else{
        status.checked = false
      }
    });
  }

  ngOnInit() {
  
    this.activatedRoute.params.subscribe(data => {
      if (data.id != undefined){
        this.validatorsOkNovoUsuario = false;
        this.registerService.getEmpresa(data.id).subscribe(empresa => {
          this.registerForm.patchValue({
            Nome_Fantasia: empresa['Nome_Fantasia'],
            Razao_Social: empresa['Razao_Social'],
            Cnpj: empresa['Cnpj'],
            tem_upload: empresa['tem_upload'],
          })
          this.empresa_id = empresa['id']
        })
      }      
    })
    //console.log(this.activatedRoute)
    
    this.registerForm = this.formBuilder.group({
      id:null,
      Nome_Fantasia: ['', 
        [
          Validators.required
        ]
      ],
      Razao_Social: ['', 
        [
          Validators.required
        ],
        //this.UserNotTakenValidatorService.checkUserNameTaken()
      ],
      Cnpj: ['', 
        [
          Validators.required
        ],
        //this.UserNotTakenValidatorService.checkEmailTaken()
      ],
      Contabilidade: null,
      tem_upload: 0,
      data_inscricao: new Date(),
      status: null
    });    
    
  }

}
