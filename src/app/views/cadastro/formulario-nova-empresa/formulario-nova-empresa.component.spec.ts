import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioNovaEmpresaComponent } from './formulario-nova-empresa.component';

describe('FormularioNovaEmpresaComponent', () => {
  let component: FormularioNovaEmpresaComponent;
  let fixture: ComponentFixture<FormularioNovaEmpresaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioNovaEmpresaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioNovaEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
