import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxMaskModule, IConfig } from 'ngx-mask'

import { FormularioNovoUsuarioComponent } from './formulario-novo-usuario/formulario-novo-usuario.component';
import { FormularioNovaEmpresaComponent } from './formulario-nova-empresa/formulario-nova-empresa.component';
import { NovaContabilidadeComponent } from './nova-contabilidade/nova-contabilidade.component';
import { NovoTreinamentoComponent } from './novo-treinamento/novo-treinamento.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;
 

@NgModule({
  declarations: [FormularioNovoUsuarioComponent, FormularioNovaEmpresaComponent, NovaContabilidadeComponent, NovoTreinamentoComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDropzoneModule,
    NgxMaskModule.forRoot(options)
  ],
  exports: [FormularioNovoUsuarioComponent]
})
export class CadastroModule { }
