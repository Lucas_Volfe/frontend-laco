import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RegisterService } from '../../../services/register.service';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-nova-contabilidade',
  templateUrl: './nova-contabilidade.component.html',
  styleUrls: ['./nova-contabilidade.component.scss']
})
export class NovaContabilidadeComponent implements OnInit {

  public file = null;
  public novoUsuario;
  public validatorsOkNovoUsuario = true;
  public registerForm: FormGroup;
  public plano: number = 1;
  public status: number = 1;
  public planos = [
    { id: 1, name: 'Plano 1', checked: true },
    { id: 2, name: 'Plano 2', checked: false },
    { id: 3, name: 'Plano 3', checked: false }
  ];
  public statusList = [
    { id: 1, name: 'Ativo', checked: true },
    { id: 2, name: 'Teste', checked: false },
    { id: 3, name: 'Desabilitado', checked: false }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private router:Router,
    private _toastr: ToastrService,
    private _contabilidadeService: RegisterService,
    config: NgbModalConfig, 
    private _route: ActivatedRoute,
    private modalService: NgbModal,
    private userService: RegisterService
    ) { }    

    registrar(){
      const contabilidade = this.registerForm.getRawValue();
      contabilidade.Plano = this.plano;      
      contabilidade.status = this.statusList[(this.status-1)].name;
      let cnpjNovaContabilidade = contabilidade.Cnpj;


      if (this._route.snapshot.paramMap.get('id')) {
        // contabilidade.id = this._route.snapshot.paramMap.get('id');
        this._contabilidadeService.createContabilidade(contabilidade, this.file).subscribe(()=>{
          this._toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
          this.router.navigate(['']);
        }, 
        err => {
          this._toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
          console.log(err)
        }) 

      }
      else{
        this._contabilidadeService.createContabilidade(contabilidade, this.file).subscribe( 
          () => {
            this._contabilidadeService.getTodasContabilidades().subscribe(data => {
              data.forEach(item => {
                if(item.Cnpj == cnpjNovaContabilidade){
                  this.novoUsuario.Contabilidade = item.id;
                  this.userService.createUser(this.novoUsuario).subscribe(()=>{
                    this._toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
                    this.router.navigate(['']);
                  })
                } 
              })            
            })
  
          }, 
          err => {
            this._toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
            console.log(err)
        });     
      }        
    }

    reciverFeedback(novoUsuario) {
      novoUsuario.Role = 2;
      novoUsuario.username = novoUsuario.email;
      this.novoUsuario = novoUsuario
      this.validatorsOkNovoUsuario = false
    }

    checkContabilidadeTaken(){
      console.log('checkContabilidadeTaken')
    }
    
    selectPlano(idPlano){    
      this.planos.forEach(plano => {
        if(plano.id == idPlano){
          this.plano = idPlano;
          plano.checked = true
        }else{
          plano.checked = false
        }
      });
    }
    selectStatus(idStatus){      
      this.statusList.forEach(status => {
        if(status.id == idStatus){
          this.status = idStatus;
          status.checked = true
        }else{
          status.checked = false
        }
      });
    }

    fileToBase64(base){
      this.file = base;
    }

    onFilesAdded(files: File[]) {
      files.forEach(file => {
        const reader = new FileReader();
        reader.readAsDataURL(file);       
        reader.onload = (e: any)=>{
          this.fileToBase64(e.target.result)  
        }
      });     
  
    }

    open(content){
      this.modalService.open(content);
    }

    ngOnInit(): void {   
      let id: number = Number(this._route.snapshot.paramMap.get('id'));
      if(id) this.validatorsOkNovoUsuario = false;
      this.registerForm = this.formBuilder.group({
        id: null,
        Nome_Fantasia: ['', 
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
        ],
        Razao_Social: ['', 
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
        ],
        Cnpj: ['', 
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
        ],
        Plano: 1,
        status: 1,
        foto: null
      });   
      
      if(id){
        this._contabilidadeService.getContabilidade(id).subscribe(data => {                 
          this.registerForm.patchValue({
            Nome_Fantasia: data.Nome_Fantasia,
            Razao_Social: data.Razao_Social,
            Cnpj: data.Cnpj,
            Plano: data.Plano['id'],
            status: data.status,
            id: data.id      
          });
          this.file = data.foto;
          this.selectPlano(data.Plano['id']);
          (data.status == 'Ativo') ?
            this.selectStatus(1) : (data.status == 'Teste') ?
              this.selectStatus(2) : this.selectStatus(3);
        })
      }
    }

}
