import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaContabilidadeComponent } from './nova-contabilidade.component';

describe('NovaContabilidadeComponent', () => {
  let component: NovaContabilidadeComponent;
  let fixture: ComponentFixture<NovaContabilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaContabilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaContabilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
