import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { RegisterService } from '../../../services/register.service';

@Component({
  selector: 'app-novo-treinamento',
  templateUrl: './novo-treinamento.component.html',
  styleUrls: ['./novo-treinamento.component.scss']
})
export class NovoTreinamentoComponent implements OnInit {

  public registerForm: FormGroup;  

  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private router:Router,
    private toastr: ToastrService,

  ) {  }

  selecionaEstado(){
    this.registerForm.patchValue({Status: !this.registerForm.get('Status').value})
  }

  onFilesAdded(files: File[]) {
     
    console.log(files)
    this.registerForm.patchValue({Conteudo: 'foi'})

  }
  onFilesRejected($event){
    console.log($event)
  }

  registrar(){
    const novaEmpresa = this.registerForm.getRawValue();
    
    // this.registerService.createEmpresa(novaEmpresa).subscribe( 
    //   () => {
    //     this.router.navigate(['']);
    //     this.toastr.success('Cadastro realizado com Sucesso!', 'Sucesso!');
    //   }, 
    //   err => {
    //     this.toastr.error('Um erro ocorreu, contate o Administrador!', 'Oops!');
    //     console.log(err)
    // });     
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      Titulo: ['',[Validators.required]],
      Descricao: ['',[Validators.required]],
      Status: false,
      Conteudo: [null,[Validators.required]]
    });    
    
  }

}
