import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardsComponent } from './dashboards/dashboards.component';
import { HistoricoUploadsComponent } from '../contabilidade/historico-uploads/historico-uploads.component';
import { VisualizarDashboardsComponent } from '../contabilidade/visualizar-dashboards/visualizar-dashboards.component';
import { ChartsService } from '../../services/charts.service';
import { HistoricoUploadsResolver } from '../contabilidade/historico-uploads/historico-uploads.resolver';
import { VisualizarDashboardsResolver } from '../contabilidade/visualizar-dashboards/visualizar-dashboards.resolver';



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Empresa'
    },
    children:[
      {
        path: '',
        component: VisualizarDashboardsComponent,
        data: {
          title: 'Empresa'
        },
        resolve: {dados: VisualizarDashboardsResolver}
      },
      {
        path: 'dashboards',
        component: DashboardsComponent,
        data:{
          title: 'Dashboards'
        }
      },
      {
        path: 'historico-uploads',
        component: HistoricoUploadsComponent,
        data:{
          title: 'Histórico de Uploads'
        },
        //resolve:{historico: HistoricoUploadsResolver}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpresaRoutingModule {}
