import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { DashboardsComponent } from './dashboards/dashboards.component';
import { EmpresaRoutingModule } from './empresa-routing.module';
import { ContabilidadeModule } from '../contabilidade/contabilidade.module';
import { HistoricoUploadsResolver } from '../contabilidade/historico-uploads/historico-uploads.resolver';
import { VisualizarDashboardsResolver } from '../contabilidade/visualizar-dashboards/visualizar-dashboards.resolver';
import { NgxLoadingModule } from 'ngx-loading';



@NgModule({
  declarations: [
    IndexComponent, 
    DashboardsComponent    
  ],
  imports: [
    CommonModule,
    EmpresaRoutingModule,
    ContabilidadeModule,
    NgxLoadingModule
  ],
  providers: [
    HistoricoUploadsResolver,
    VisualizarDashboardsResolver
  ]
})
export class EmpresaModule { }
